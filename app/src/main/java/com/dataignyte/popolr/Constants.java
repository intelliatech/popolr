package com.dataignyte.popolr;

public class Constants {
    public static final String DEPARTMENT_NAME = "departemntName";
    public static final String DEPARTMENT_ID = "departmentId";
    public static final String ITEM_ID = "itemId";
    public static final String SUB_CATEGORY_ID = "subCategoryId";
    public static final String SUB_CATEGORY_NAME = "subCategoryName";
    public static final String ID = "id";
    public static final String RUPEE = "\u20B9";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_PHONE = "user_phone";
    public static final String TOKEN = "token";
    public static final String ITEMS = "items";
    public static final String ADD_ITEM = "add_item";
    public static final String INCREASE_ITEM = "increase_item";
    public static final String DECREASE_ITEM = "decrease_item";
    public static final String AUTHORIZATION = "Authorization";
    public static final String DIRECT_FROM_HOME = "directFromHome";
    public static final String DELIVERY_ADDRESS_ID = "delivery_address_id";
    public static final String ORDER_ID = "orderId";
    public static final String PLACED = "placed";
    public static final String TRANSIT = "transit";
    public static final String DELIVERED = "delivered";
    public static final String ORDER_FLAG = "orderFlag";
    public static final String FRAGMENT_FLAG = "fragmentFlag";
    public static final String FRAGMENT_ORDER_LIST = "fragmentOrderList";
    public static final String FRAGMENT_MY_TRIBES = "fragmentMyTribes";


    public static class URL {
        public static final String BASE_URL = "http://api.popolr.com/";
        public static final String GET_DEPARTMENTS = "api/departments";
        public static final String GET_CATEGORY_LIST_BY_DEPARTMENT = "/api/categories/departments";
        public static final String GET_SUB_CATEGORY_LIST_BY_CATEGORY = "/api/subcategories/categories";
        public static final String GET_ITEM_LIST_BY_SUB_CATEGORY = "/api/items/subcategories";
        public static final String REGISTER_USER = "/api/users";
        public static final String CHANGE_PASSWORD = "/api/users/password";
        public static final String GET_ITEM_DETAILS = "/api/items";
        public static final String GET_SPECIAL_ITEMS = "/api/items/special";
        public static final String GET_SEARCH_ITEM_BY_NAME = "/api/items";
        public static final String USER_lOGIN = "/api/auth";
        public static final String GET_USER_DETAILS = "/api/users/auth";
        public static final String GET_CART = "/api/carts";
        public static final String ADD_ADDRESS = "/api/addresses";
        public static final String GET_ADDRESSES = "/api/addresses";
        public static final String DELETE_ADDRESS = "/api/addresses";
        public static final String GET_DELIVERY_SLOTS = "/api/deliverySlots";
        public static final String GET_PAYMENT_MODES = "/api/paymentModes";
        public static final String ORDER_URL = "/api/orders";
    }

    public static class PermissionCodes {
        public static final int PERMISSION_REQUEST_CODE = 100;
        public static final int CAMERA__REQUEST_CODE = 01;
        public static final int GALLERY_REQUEST_CODE = 02;
    }
}
