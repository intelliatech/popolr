package com.dataignyte.popolr.helper;

import android.app.Activity;
import android.app.Service;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class Utility {
    private static final String TAG = Utility.class.getSimpleName();
    public static String imageFilePath = "";
    private static Toast toast;
    private static Snackbar snackbar;

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public static boolean checkRuntimePermission(Context context, String... permissions) {
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public static void setFocusAndOpenKeyBoard(Context context, EditText editText) {
        editText.setFocusable(true);
        editText.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, 0);
    }

    public static String convertDateFormat(String inputDateFormat, String outPutDateFormat, String tempDate) {
        Date date = null;
        String formatDate = "";
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(inputDateFormat);
            date = formatter.parse(tempDate);
            formatDate = new SimpleDateFormat(outPutDateFormat).format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formatDate;
    }

    // ----------------------------Toast
    public static void ShowToastMessage(Context context, String message) {
//        if (toast != null)
//            toast.cancel();
//
//        toast = Toast.makeText(ctactivity, message, Toast.LENGTH_SHORT);
//        toast.setGravity(Gravity.CENTER, 0, 0);
//        toast.show();
//        Snackbar snackbar;
        if (snackbar != null)
            snackbar.dismiss();

        View parentLayout = ((Activity) context).findViewById(android.R.id.content);
        snackbar = Snackbar.make(parentLayout, message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        snackbar.show();
    }

    public static void ShowToastMessage(Context context, int message) {
//        if (toast != null)
//            toast.cancel();
//        toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
//        toast.setGravity(Gravity.CENTER, 0, 0);
//        toast.show();
        if (snackbar != null)
            snackbar.dismiss();

        View parentLayout = ((Activity) context).findViewById(android.R.id.content);
        snackbar = Snackbar.make(parentLayout, message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        snackbar.show();
    }

    public static void showSnackbar(Context context, String message) {
        Snackbar snackbar;
        View parentLayout = ((Activity) context).findViewById(android.R.id.content);
        snackbar = Snackbar.make(parentLayout, message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        snackbar.show();
    }

    public static String geNonNullString(String value) {
        if (value != null && !value.equalsIgnoreCase("null"))
            return value;
        else
            return "";
    }

    public static String get2DecimalStringOfDouble(double value) {
        DecimalFormat df = new DecimalFormat("####0.00");
        return df.format(value);
    }

    public static String get2DecimalStringOfFloat(float value) {
        DecimalFormat df = new DecimalFormat("####0.00");
        return df.format(value);
    }

//    public static String getFirebaseToken(){
//        final String[] token = {""};
//        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
//            @Override
//            public void onSuccess(InstanceIdResult instanceIdResult) {
//                token[0] =  instanceIdResult.getToken();
//                Log.e( "onSuccess: ", instanceIdResult.getToken() );
//            }
//        });
//        return token[0];
//    }

    public static String findDifferenceInTime(String start_date, String end_date) {
        if (start_date.length() != 0 && end_date.length() != 0) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            try {
                Date d1 = sdf.parse(start_date);
                Date d2 = sdf.parse(end_date);
                long difference_In_Time = d2.getTime() - d1.getTime();
                long difference_In_Seconds = (difference_In_Time / 1000) % 60;
                long difference_In_Minutes = (difference_In_Time / (1000 * 60)) % 60;
                long difference_In_Hours = (difference_In_Time / (1000 * 60 * 60)) % 24;
                if (difference_In_Hours == 0 && (difference_In_Minutes % 60) == 0 && (difference_In_Seconds % 60) == 0)
                    return "00:00:00";
                else if (difference_In_Hours == 0 && (difference_In_Minutes % 60) == 0)
                    return "00:00:" + (difference_In_Seconds % 60);
                else if (difference_In_Hours == 0)
                    return "00:" + (difference_In_Minutes % 60) + ":" + (difference_In_Seconds % 60);
                else
                    return difference_In_Hours + ":" + (difference_In_Minutes % 60) + ":" + (difference_In_Seconds % 60);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public static String getStringSizeLengthOfFile(long size) {
        DecimalFormat df = new DecimalFormat("0.00");
        float sizeKb = 1024.0f;
        float sizeMb = sizeKb * sizeKb;
        float sizeGb = sizeMb * sizeKb;
        float sizeTerra = sizeGb * sizeKb;
        if (size < sizeMb)
            return df.format(size / sizeKb) + " Kb";
        else if (size < sizeGb)
            return df.format(size / sizeMb) + " Mb";
        else if (size < sizeTerra)
            return df.format(size / sizeGb) + " Gb";
        return "";
    }

    public static void openWhatsapp(Context context, String phone, String message) {
        if (phone.length() <= 10)
            phone = "+91" + phone;
        PackageManager packageManager = context.getPackageManager();
        Intent i = new Intent(Intent.ACTION_VIEW);

        try {
            String url = "https://api.whatsapp.com/send?phone=" + phone + "&text=" + URLEncoder.encode(message, "UTF-8");
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                context.startActivity(i);
            } else
                Utility.ShowToastMessage(context, "Sorry did not found Whatsapp account!");
        } catch (Exception e) {
            Utility.ShowToastMessage(context, e.getMessage());
            e.printStackTrace();
        }
    }

    public static String getFilePath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                } else if ("pdf".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static Bitmap decodeFile(File f) {         /*-------------------To decode image file in bitmap--------------------*/
        Bitmap b = null;
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(f);
            BitmapFactory.decodeStream(fis, null, o);
            fis.close();
        } catch (Exception e) {
            Log.e(TAG, "decodeFile: " + e.getMessage());
        }
        int IMAGE_MAX_SIZE = 1024;
        int scale = 1;
        if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
            scale = (int) Math.pow(2, (int) Math.ceil(Math.log(IMAGE_MAX_SIZE /
                    (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
        }
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        try {
            fis = new FileInputStream(f);
            b = BitmapFactory.decodeStream(fis, null, o2);
            fis.close();
        } catch (Exception e) {
            Log.e(TAG, "decodeFile: " + e.getMessage());
        }
        try {
            FileOutputStream out = new FileOutputStream(f);
            b.compress(Bitmap.CompressFormat.PNG, 60, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            Log.e(TAG, "decodeFile: " + e.getMessage());
        }
        return b;
    }

    public static void selectimag(Context context, Fragment fragment, String str_message, CharSequence[] options) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(str_message);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(context.getString(R.string.from_camera))) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(context, CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            if (pictureIntent.resolveActivity(context.getPackageManager()) != null) {
                                File photoFile = null;
                                try {
                                    photoFile = createImageFile(context);
                                } catch (IOException e) {
                                    return;
                                }
                                Uri photoUri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", photoFile);
                                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                                if (fragment == null)
                                    ((Activity) context).startActivityForResult(pictureIntent, Constants.PermissionCodes.CAMERA__REQUEST_CODE);
                                else
                                    fragment.startActivityForResult(pictureIntent, Constants.PermissionCodes.CAMERA__REQUEST_CODE);
                            }
                        }
                    }
                } else if (options[item].equals(context.getString(R.string.from_gallery))) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(context, CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            if (fragment == null)
                                ((Activity) context).startActivityForResult(intent, Constants.PermissionCodes.GALLERY_REQUEST_CODE);
                            else
                                fragment.startActivityForResult(intent, Constants.PermissionCodes.GALLERY_REQUEST_CODE);
                        }
                    }
                } else if (options[item].equals(context.getString(R.string.close))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private static File createImageFile(Context context) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".png", storageDir);
        imageFilePath = image.getAbsolutePath();
        return image;
    }

    public static String getCartItemsCount(Context context) {
        Log.e("getCartItemsCount--", SharedPref.getSharePrefItemsArraylist(context).size() + "");
        String size = "";
        if (SharedPref.getSharePrefItemsArraylist(context) != null)
            size = String.valueOf(SharedPref.getSharePrefItemsArraylist(context).size());
        else
            size = "0";
        return size;
    }
    public static boolean isValidMobileNumber(String s) {
        // The given argument to compile() method
        // is regular expression. With the help of
        // regular expression we can validate mobile
        // number.
        // 1) Begins with 0 or 91
        // 2) Then contains 7 or 8 or 9.
        // 3) Then contains 9 digits
        Pattern p = Pattern.compile("(0/91)?[6-9][0-9]{9}");
        // Pattern class contains matcher() method
        // to find matching between given number
        // and regular expression
        Matcher m = p.matcher(s);
        return (m.find() && m.group().equals(s));
    }
}
