package com.dataignyte.popolr.helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;

import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.retrofit.model.response.ItemDetails;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class SharedPref {

    public static final String PREFERENCE = "My Shared Prefrence";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String TAG = SharedPref.class.getSimpleName();
    private static Context mContext;
    private static ArrayList<ItemDetails> arrayListItemDetails = new ArrayList<>();
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    int PRIVATE_MODE = 0;


    public SharedPref(Context mContext) {
        this.mContext = mContext;
        pref = mContext.getSharedPreferences(PREFERENCE, PRIVATE_MODE);
        editor = pref.edit();
    }

    public static void setSharedPreference(Context context, String name, String value) {
        mContext = context;
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(name, value);
        editor.apply();
    }

    public static void setSharedPreference(Context context, String name, int value) {
        mContext = context;
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        Log.e("setSharedPreference: ", String.valueOf(value));
        editor.putInt(name, value);
        editor.apply();
    }

    public static int getSharedPreferences(Context context, String name, int defaultValue) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        return settings.getInt(name, defaultValue);
    }

    public static String getSharedPreferences(Context context, String name) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        return settings.getString(name, "");
    }

    public static void removepreference(Context context, String name) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        settings.edit().remove(name).apply();
    }

    public static void saveItemInSharePref(Context context, ItemDetails itemDetails, String request) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);

        /*Check whether item already present in arraylist or not*/
        if (settings.contains(Constants.ITEMS)) {
            arrayListItemDetails = getSharePrefItemsArraylist(context);
            boolean found = false;
            for (int i = 0; i < arrayListItemDetails.size(); i++) {
                if (itemDetails.getId().equalsIgnoreCase(arrayListItemDetails.get(i).getId())) {
                    Log.i(TAG, "-----" + itemDetails.getQuantity());
                    switch (request) {
                        case Constants.ADD_ITEM:
                            showSnackbar(context, context.getResources().getString(R.string.item_added_to_cart));
                            arrayListItemDetails.get(i).setQuantity((itemDetails.getQuantity()));
                            break;
                        case Constants.INCREASE_ITEM:
                            showSnackbar(context, context.getResources().getString(R.string.item_quantity_change));
                            arrayListItemDetails.get(i).setQuantity((itemDetails.getQuantity()));
                            break;
                        case Constants.DECREASE_ITEM:
                            if (arrayListItemDetails.get(i).getQuantity() == 1) {
                                arrayListItemDetails.remove(i);
                                showSnackbar(context, context.getResources().getString(R.string.item_removed_from_cart));
                            } else {
                                arrayListItemDetails.get(i).setQuantity((itemDetails.getQuantity()));
                                showSnackbar(context, context.getResources().getString(R.string.item_quantity_change));
                            }
                            break;
                    }
                    found = true;
                    Log.i(TAG, "isItemAlreadyPresent: true");
                    break;
                }

            }
//            showSnackbar(context,"Shared Pref message");
            if (!found) {
                arrayListItemDetails.add(itemDetails);
                showSnackbar(context, context.getResources().getString(R.string.item_added_to_cart));
                Log.i(TAG, "isItemAlreadyPresent: false");
            }
        } else {
            arrayListItemDetails.add(itemDetails);
            showSnackbar(context, context.getResources().getString(R.string.item_added_to_cart));
            Log.i(TAG, "itemsSharePrefInitialised");
        }
        /*Saving Updated sharePref as arraylist*/
        saveItemsArrayListInSharedPref(context, arrayListItemDetails);
    }

    public static void clearPreference(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        settings.edit().clear().apply();
    }

    public static boolean contains(Context context, String name) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        return settings.contains(name);
    }

    public static void saveItemsArrayListInSharedPref(Context context, ArrayList<ItemDetails> arrayListItemDetails) {

//        if (arrayListItemDetails != null && arrayListItemDetails.size() != 0)
//            Utility.ShowToastMessage(context, context.getResources().getString(R.string.cart_item_updated));

        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);
        SharedPreferences.Editor editor = settings.edit();
        Gson gson = new Gson();
        String json = gson.toJson(arrayListItemDetails);
        editor.putString(Constants.ITEMS, json);
        editor.apply();
        Log.i(TAG, "updatedSharePref: " + arrayListItemDetails.toString());

    }

    public static ArrayList<ItemDetails> getSharePrefItemsArraylist(Context context) {
        Gson gson = new Gson();
        SharedPreferences settings = context.getSharedPreferences(PREFERENCE, 0);

        if (SharedPref.contains(context, Constants.ITEMS)) {
            String json = settings.getString(Constants.ITEMS, null);
            Type type = new TypeToken<ArrayList<ItemDetails>>() {
            }.getType();
            return gson.fromJson(json, type);
        } else return new ArrayList<ItemDetails>();
    }

    private static void showSnackbar(Context context, String message) {
        Snackbar snackbar;
        View parentLayout = ((Activity) context).findViewById(android.R.id.content);
        snackbar = Snackbar.make(parentLayout, message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        snackbar.show();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }


}