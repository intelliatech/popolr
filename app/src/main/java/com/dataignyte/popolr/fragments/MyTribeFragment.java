package com.dataignyte.popolr.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.helper.OnListItemClicked;
import com.dataignyte.popolr.helper.SharedPref;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.APICaller.RetrofitClient;
import com.dataignyte.popolr.retrofit.model.response.UserDetailsResponse;

import de.blox.treeview.BaseTreeAdapter;
import de.blox.treeview.TreeNode;
import de.blox.treeview.TreeView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyTribeFragment extends BaseFragment implements View.OnClickListener, OnListItemClicked {

    private static final String TAG = MyTribeFragment.class.getSimpleName();
    BaseTreeAdapter adapter;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_tribes, container, false);
        initialise(view);
        return view;
    }

    private void initialise(View view) {
        context = getContext();
        TreeView treeView = view.findViewById(R.id.treeview);


        adapter = new BaseTreeAdapter<ViewHolder>(context, R.layout.row_my_tribes) {
            @NonNull
            @Override
            public ViewHolder onCreateViewHolder(View view) {
                return new ViewHolder(view);
            }

            @Override
            public void onBindViewHolder(ViewHolder viewHolder, Object data, int position) {
                viewHolder.textUserName.setText(((UserDetailsResponse) data).getFirstName()
                        + " " + ((UserDetailsResponse) data).getLastName());
                viewHolder.txvOrderTotal.setText(context.getResources().getString(R.string.total_amount_order_placed)
                        + " - " + Utility.get2DecimalStringOfDouble(((UserDetailsResponse) data).getOrderTotal()));
                viewHolder.txvDirect.setText(((UserDetailsResponse) data).getDirect() + "");
            }
        };

        treeView.setAdapter(adapter);
        callGetMyTribesAPI();


        // example tree
//        TreeNode rootNode = new TreeNode(getNodeText());
//        rootNode.addChild(new TreeNode(getNodeText()));
//        final TreeNode child3 = new TreeNode(getNodeText());
//        child3.addChild(new TreeNode(getNodeText()));
//        final TreeNode child6 = new TreeNode(getNodeText());
//        child6.addChild(new TreeNode(getNodeText()));
//        child6.addChild(new TreeNode(getNodeText()));
//        child3.addChild(child6);
//        rootNode.addChild(child3);
//        final TreeNode child4 = new TreeNode(getNodeText());
//        child4.addChild(new TreeNode(getNodeText()));
//        child4.addChild(new TreeNode(getNodeText()));
//        rootNode.addChild(child4);
//        adapter.setRootNode(rootNode);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    @Override
    public void onItemClicked(int position, View view, String value) {
        switch (view.getId()) {

        }
    }

    private void callGetMyTribesAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            RetrofitClient.getAPIService().getUserDetails(SharedPref.getSharedPreferences(context, Constants.TOKEN))
                    .enqueue(new Callback<UserDetailsResponse>() {
                        @Override
                        public void onResponse(Call<UserDetailsResponse> call, Response<UserDetailsResponse> response) {
                            stopProgressHud(context);
                            if (response.code() == 200 && response.body() != null) {
                                setTreeViewAdapter(response.body());
                            } else {
                                stopProgressHud(context);
                                Log.e(TAG, response.toString());
                                Utility.ShowToastMessage(context, response.message());
                            }
                        }
                        @Override
                        public void onFailure(Call<UserDetailsResponse> call, Throwable t) {
                            stopProgressHud(context);
                            Log.e(TAG, t.getMessage(), t);
                        }
                    });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

    private void setTreeViewAdapter(UserDetailsResponse userDetailsResponse) {

        TreeNode rootNode = new TreeNode(userDetailsResponse);
        for (int i = 0; i < userDetailsResponse.getChild().size(); i++) {
            TreeNode child = new TreeNode(userDetailsResponse.getChild().get(i));
            rootNode.addChild(child);
        }
        adapter.setRootNode(rootNode);
    }

    private class ViewHolder {
        TextView textUserName, txvOrderTotal, txvDirect, txvSubCordinates;

        ViewHolder(View view) {
            textUserName = view.findViewById(R.id.textUserName);
            txvOrderTotal = view.findViewById(R.id.txvOrderTotal);
            txvSubCordinates = view.findViewById(R.id.txvSubCordinates);
            txvDirect = view.findViewById(R.id.txvDirect);

        }
    }
}
