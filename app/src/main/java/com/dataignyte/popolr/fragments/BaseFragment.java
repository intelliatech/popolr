package com.dataignyte.popolr.fragments;

import android.content.Context;

import androidx.fragment.app.Fragment;

import com.dataignyte.popolr.activities.BaseActivity;

public class BaseFragment  extends Fragment {
    public void startProgressHud(Context context){
        if (((BaseActivity) context)!=null)
            ((BaseActivity) context).startProgressHud();
    }

    public void stopProgressHud(Context context){
        if (((BaseActivity) context)!=null)
            ((BaseActivity) context).stopProgressHud();
    }
}
