package com.dataignyte.popolr.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.activities.CheckOutActivity;
import com.dataignyte.popolr.adapters.OrderSummaryAdapter;
import com.dataignyte.popolr.helper.SharedPref;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.APICaller.RetrofitClient;
import com.dataignyte.popolr.retrofit.model.response.CartResponse;
import com.dataignyte.popolr.retrofit.model.response.ItemDetails;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeliveryDetailsFragment extends BaseFragment  {

    private static final String TAG = DeliveryDetailsFragment.class.getSimpleName();
    private Context context;
    private RecyclerView orderSummaryRv;
    private ArrayList<ItemDetails> itemsArrayList;
    private OrderSummaryAdapter orderSummaryAdapter;
    public static boolean orderItemsPresent = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_delivery_details, container, false);
        initialise(view);
        return view;
    }

    private void initialise(View view) {
        context = getContext();
        orderSummaryRv = (RecyclerView) view.findViewById(R.id.orderSummaryRv);
        orderSummaryRv.setLayoutManager(new LinearLayoutManager(context));
        getMyCart();
    }

    private void getMyCart() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            RetrofitClient.getAPIService().getMyCart(SharedPref.getSharedPreferences(context, Constants.TOKEN)).enqueue(new Callback<CartResponse>() {
                @Override
                public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                    stopProgressHud(context);
                    if (response.code() == 200 && response.body() != null && response.body().getCart() != null) {
                        itemsArrayList = response.body().getCart().getItems();
                        orderSummaryAdapter = new OrderSummaryAdapter(context, itemsArrayList);
                        orderSummaryRv.setAdapter(orderSummaryAdapter);

                        if (itemsArrayList.size()!=0)
                            orderItemsPresent = true;
                        else
                            orderItemsPresent = false;

                        CheckOutActivity.txvSubTotal.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfDouble(response.body().getCart().getSubTotal()));
                        CheckOutActivity.txvDelCharges.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfDouble(response.body().getCart().getDeliveryCharge()));
                        CheckOutActivity.txvDiscountTotal.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfDouble(response.body().getCart().getDiscountTotal()));
                        CheckOutActivity.txvGrandTotal.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfDouble(response.body().getCart().getTotal()));
                        CheckOutActivity.txvTotalAmount.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfDouble(response.body().getCart().getTotal()));
                    } else {
                        Log.e(TAG, response.toString());
                        Utility.ShowToastMessage(context, response.message());
                        orderItemsPresent = false;
                    }
                }

                @Override
                public void onFailure(Call<CartResponse> call, Throwable t) {
                    stopProgressHud(context);
                    Log.e(TAG, t.getMessage(), t);
                    orderItemsPresent = false;
                }
            });
        } else {
            orderItemsPresent = false;
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
        }
    }
}