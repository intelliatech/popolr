package com.dataignyte.popolr.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.activities.CategoryListActivity;
import com.dataignyte.popolr.activities.HomePageActivity;
import com.dataignyte.popolr.adapters.DepartmentsListAdapter;
import com.dataignyte.popolr.adapters.OffersListAdapter;
import com.dataignyte.popolr.helper.OnListItemClicked;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.APICaller.RetrofitClient;
import com.dataignyte.popolr.retrofit.model.response.Department;
import com.dataignyte.popolr.retrofit.model.response.DepartmentList;
import com.dataignyte.popolr.retrofit.model.response.ItemDetails;
import com.dataignyte.popolr.retrofit.model.response.ItemsList;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DepartmentListFragment extends BaseFragment implements OnListItemClicked {

    private final String TAG = DepartmentListFragment.class.getSimpleName();
    private Context context;
    private RecyclerView offersListRv, departmentsListRv;
    private TextView txvNooffersFound, txvNoDepartsmentsFound;
    private ArrayList<Department> departmentArrayList = new ArrayList<>();
    private ArrayList<ItemDetails> itemsArrayList = new ArrayList<>();
    private DepartmentsListAdapter departmentsListAdapter;
    private OffersListAdapter offersListAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_department_list, container, false);
        initialise(view);
        return view;
    }

    private void initialise(View view) {
        context = getContext();
        offersListRv = (RecyclerView) view.findViewById(R.id.offersListRv);
        txvNooffersFound = (TextView) view.findViewById(R.id.txvNooffersFound);
        departmentsListRv = (RecyclerView) view.findViewById(R.id.departmentsListRv);
        txvNoDepartsmentsFound = (TextView) view.findViewById(R.id.txvNoDepartsmentsFound);

        /*Hiding offer list temporary*/
        offersListRv.setVisibility(View.GONE);

        /*Call get all department API*/
        callGetAllDepartmentsAPI();

        offersListRv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        departmentsListRv.setLayoutManager(new GridLayoutManager(context, 2));

        setFillter();
    }

    private void setFillter() {
        HomePageActivity.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (departmentsListAdapter!=null)
                    departmentsListAdapter.getFilter().filter(s.toString());
            }
        });
    }

    @Override
    public void onItemClicked(int position, View view, String value) {
        switch (view.getId()) {
            case R.id.cvBtn:
                context.startActivity(new Intent(context, CategoryListActivity.class)
                        .putExtra(Constants.DEPARTMENT_ID, departmentArrayList.get(position).getId())
                        .putExtra(Constants.DEPARTMENT_NAME, departmentArrayList.get(position).getName()));
                break;
        }
    }

    private void callGetAllDepartmentsAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            RetrofitClient.getAPIService().callGetAllDepartmentAPI().enqueue(new Callback<DepartmentList>() {
                @Override
                public void onResponse(Call<DepartmentList> call, Response<DepartmentList> response) {
                    stopProgressHud(context);
                    if (response.code() == 200 && response.body() != null && response.body().getDepartments() != null) {
                        departmentArrayList = response.body().getDepartments();
                        departmentsListAdapter = new DepartmentsListAdapter(context, departmentArrayList, DepartmentListFragment.this);
                        departmentsListRv.setAdapter(departmentsListAdapter);

                        /*Hidding it initially*/
//                        getSpecialItemsAPI();
                    } else {
                        txvNoDepartsmentsFound.setVisibility(View.VISIBLE);
                        Log.e(TAG, response.toString());
                        Utility.ShowToastMessage(context, response.message());
                    }

                    if (departmentArrayList.size()!=0)
                        txvNoDepartsmentsFound.setVisibility(View.GONE);
                    else
                        txvNoDepartsmentsFound.setVisibility(View.VISIBLE);

                }
                @Override
                public void onFailure(Call<DepartmentList> call, Throwable t) {
                    stopProgressHud(context);
                    Log.e(TAG, t.getMessage(), t);
                }
            });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

    private void getSpecialItemsAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            RetrofitClient.getAPIService().getSpecialItemsAPI().enqueue(new Callback<ItemsList>() {
                @Override
                public void onResponse(Call<ItemsList> call, Response<ItemsList> response) {
                    stopProgressHud(context);
                    if (response.code() == 200 && response.body() != null && response.body().getItems() != null) {
                        itemsArrayList = response.body().getItems();
                        offersListAdapter = new OffersListAdapter(context, itemsArrayList);
                        offersListRv.setAdapter(offersListAdapter);
                    } else {
                        txvNoDepartsmentsFound.setVisibility(View.VISIBLE);
                        Log.e(TAG, response.toString());
                        Utility.ShowToastMessage(context, response.message());
                    }

                    if (itemsArrayList.size()!=0) {
                        offersListRv.setVisibility(View.VISIBLE);
                        txvNooffersFound.setVisibility(View.GONE);
                    } else {
                        offersListRv.setVisibility(View.GONE);
                        txvNooffersFound.setVisibility(View.VISIBLE);
                    }

                }
                @Override
                public void onFailure(Call<ItemsList> call, Throwable t) {
                    stopProgressHud(context);
                    Log.e(TAG, t.getMessage(), t);
                }
            });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }
}
