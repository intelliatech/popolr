package com.dataignyte.popolr.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.activities.HomePageActivity;
import com.dataignyte.popolr.databinding.FragmentAddReferralBinding;
import com.dataignyte.popolr.helper.SharedPref;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.APICaller.RetrofitClient;
import com.dataignyte.popolr.retrofit.model.request.ReferralUserRequest;
import com.dataignyte.popolr.retrofit.model.response.SucessResult;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddReferralFragment extends BaseFragment implements View.OnClickListener {
    private final String TAG = AddReferralFragment.class.getSimpleName();
    private FragmentAddReferralBinding binding;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentAddReferralBinding.inflate(inflater, container, false);
        initialise();
        return binding.getRoot();
    }


    private void initialise() {
        context = getContext();
        binding.matBtnSave.setOnClickListener(this);

        ArrayList<String> lstGender = new ArrayList<>();
        lstGender.add("Male");
        lstGender.add("Female");
        lstGender.add("Prefer Not To Answer");
        ArrayAdapter arrayAdapter = new ArrayAdapter<String>(
                context,
                android.R.layout.simple_list_item_1, lstGender);
        binding.acGender.setAdapter(arrayAdapter);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.matBtnSave:
                if (validateFromData())
                    callAddReferralAPI();
                break;
        }
    }


    private boolean validateFromData() {

        boolean isValidate = false;
        if (binding.edtFirstName.getText().toString().trim().isEmpty()) {
            Utility.showSnackbar(context, getResources().getString(R.string.enter_first_name));
        } else if (binding.edtLastName.getText().toString().trim().isEmpty()) {
            Utility.showSnackbar(context, getResources().getString(R.string.enter_last_name));
        } else if (binding.edtNumber.getText().toString().trim().isEmpty()) {
            Utility.showSnackbar(context, getResources().getString(R.string.enter_mobile_number));
        } else if (binding.edtNumber.getText().toString().trim().length() < 10) {
            Utility.ShowToastMessage(context, getString(R.string.enter_valid_number));
        } else if (!Utility.isValidMobileNumber(binding.edtNumber.getText().toString().trim())) {
            Utility.ShowToastMessage(context, getString(R.string.enter_valid_number));
        } else if (binding.edtEmail.getText().toString().trim().isEmpty()) {
            Utility.showSnackbar(context, getResources().getString(R.string.enter_email_adress));
        } else if (!binding.edtEmail.getText().toString().trim().isEmpty()
                && !Patterns.EMAIL_ADDRESS.matcher(binding.edtEmail.getText().toString().trim()).matches()) {
            Utility.ShowToastMessage(context, getString(R.string.enter_valid_email));
        } else if (binding.acGender.getText().toString().trim().isEmpty()) {
            Utility.showSnackbar(context, getResources().getString(R.string.please_select_gender));
        } else {
            isValidate = true;
        }
        return isValidate;
    }


    private void callAddReferralAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            ReferralUserRequest request = new ReferralUserRequest();
            request.setFirstName(binding.edtFirstName.getText().toString().trim());
            request.setLastName(binding.edtLastName.getText().toString().trim());
            request.setGender(binding.acGender.getText().toString().trim());
            request.setEmail(binding.edtEmail.getText().toString().trim());
            request.setPhoneNumber(Long.parseLong(binding.edtNumber.getText().toString().trim()));
            request.setParentId(SharedPref.getSharedPreferences(context, Constants.TOKEN));

            RetrofitClient.getAPIService().createReferralUser(request).enqueue(new Callback<SucessResult>() {
                @Override
                public void onResponse(Call<SucessResult> call, Response<SucessResult> response) {
                    stopProgressHud(context);
                    if (response.code() == 200 && response.body() != null) {
                        Utility.showSnackbar(context, context.getResources().getString(R.string.referral_user_added_successfully_ask_them_to_authenticate_and_use_services));
                        context.startActivity(new Intent(context, HomePageActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK & Intent.FLAG_ACTIVITY_NEW_TASK)
                                .putExtra(Constants.FRAGMENT_FLAG, Constants.FRAGMENT_MY_TRIBES));
                        ((Activity) context).finishAffinity();

                    } else {
                        Log.e(TAG, response.message());
                        Utility.showSnackbar(context, response.message());
                    }
                }

                @Override
                public void onFailure(Call<SucessResult> call, Throwable t) {
                    stopProgressHud(context);
                    Log.e(TAG, t.getMessage(), t);
                }
            });
        } else
            Utility.showSnackbar(context, getResources().getString(R.string.no_internet_connection));
    }
}
