package com.dataignyte.popolr.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.activities.CheckOutActivity;
import com.dataignyte.popolr.activities.HomePageActivity;
import com.dataignyte.popolr.adapters.AddressListAdapter;
import com.dataignyte.popolr.adapters.DeliverySlotAdapter;
import com.dataignyte.popolr.adapters.PaymentModesAdapter;
import com.dataignyte.popolr.helper.OnListItemClicked;
import com.dataignyte.popolr.helper.SharedPref;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.APICaller.RetrofitClient;
import com.dataignyte.popolr.retrofit.model.CreateOrderRequest;
import com.dataignyte.popolr.retrofit.model.PaymentModeModel;
import com.dataignyte.popolr.retrofit.model.PaymentModesResponse;
import com.dataignyte.popolr.retrofit.model.response.AddressModal;
import com.dataignyte.popolr.retrofit.model.response.DeliverySlotModal;
import com.dataignyte.popolr.retrofit.model.response.DeliverySlotsResponse;
import com.dataignyte.popolr.retrofit.model.response.GetAddressResponse;
import com.dataignyte.popolr.retrofit.model.response.SucessResult;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentDetailsFragment extends BaseFragment implements OnListItemClicked, PopupMenu.OnMenuItemClickListener {

    private static final String TAG = PaymentDetailsFragment.class.getSimpleName();
    public String addressId = "", deliverySlotId = "", paymentModeId = "", date = "";
    private Context context;
    private RecyclerView addressesRv, deliverySlotsRv, paymentModesRV;
    private ArrayList<AddressModal> addressArrayList;
    private ArrayList<DeliverySlotModal> deliverySlotsArrayList;
    private ArrayList<PaymentModeModel> paymentModesArrayList;
    private DeliverySlotAdapter deliverSlotAdapter;
    private AddressListAdapter addressListAdapter;
    private PaymentModesAdapter paymentModesAdapter;
    private PopupMenu addressOptionDialogue;
    private int selectedAddressPosition = 0;
    private Dialog dialog;
    private TextView txvDeliverySlot;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_details, container, false);
        initialise(view);
        return view;
    }

    private void initialise(View view) {
        context = getContext();
        txvDeliverySlot = (TextView) view.findViewById(R.id.txvDeliverySlot);
        addressesRv = (RecyclerView) view.findViewById(R.id.addressesRv);
        deliverySlotsRv = (RecyclerView) view.findViewById(R.id.deliverySlotsRv);
        paymentModesRV = (RecyclerView) view.findViewById(R.id.paymentModesRV);
        addressesRv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        deliverySlotsRv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        paymentModesRV.setLayoutManager(new LinearLayoutManager(context));
        getMyAddressess();

        if (SharedPref.getSharedPreferences(context, Constants.DELIVERY_ADDRESS_ID) != null)
            addressId = SharedPref.getSharedPreferences(context, Constants.DELIVERY_ADDRESS_ID);

        ((TextView) view.findViewById(R.id.txvAddAdress)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddAddressDialog();
            }
        });
        setDate();
    }

    public void setDate() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, 1);
        date = new SimpleDateFormat("EEE, dd, MMM, yyyy").format(c.getTime());
        txvDeliverySlot.setText(date);
    }

    public void showAddAddressDialog() {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_address);

        EditText edtApartmentNo = (EditText) dialog.findViewById(R.id.edtApartmentNo);
        EditText edtLocalityName = (EditText) dialog.findViewById(R.id.edtLocalityName);
        EditText edtCityName = (EditText) dialog.findViewById(R.id.edtCityName);
        EditText edtStateName = (EditText) dialog.findViewById(R.id.edtStateName);
        EditText edtPincode = (EditText) dialog.findViewById(R.id.edtPincode);
        ((ImageView) dialog.findViewById(R.id.ivBtnClose)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ((TextView) dialog.findViewById(R.id.txvButtonAdd)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edtApartmentNo.getText().toString().trim().isEmpty())
                    Utility.showSnackbar(context, context.getResources().getString(R.string.enter_your_apartment_flat_no));
                else if (edtLocalityName.getText().toString().trim().isEmpty())
                    Utility.showSnackbar(context, context.getResources().getString(R.string.enter_your_locality_name));
                else if (edtCityName.getText().toString().trim().isEmpty())
                    Utility.showSnackbar(context, context.getResources().getString(R.string.enter_your_city_name));

                else if (edtStateName.getText().toString().trim().isEmpty())
                    Utility.showSnackbar(context, context.getResources().getString(R.string.enter_your_state_name));

                else if (edtPincode.getText().toString().trim().isEmpty())
                    Utility.showSnackbar(context, context.getResources().getString(R.string.enter_your_pin_code));

                else {
                    AddressModal object = new AddressModal();
                    object.setLine1(edtApartmentNo.getText().toString().trim());
                    object.setLine2(edtLocalityName.getText().toString().trim());
                    object.setCity(edtCityName.getText().toString().trim());
                    object.setState(edtStateName.getText().toString().trim());
                    object.setPincode(edtPincode.getText().toString().trim());
                    callAddAddressAPI(object);
                }
            }
        });
        dialog.show();

        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void callAddAddressAPI(AddressModal requestObject) {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            RetrofitClient.getAPIService().callAddAddressAPI(SharedPref.getSharedPreferences(context, Constants.TOKEN), requestObject)
                    .enqueue(new Callback<AddressModal>() {
                        @Override
                        public void onResponse(Call<AddressModal> call, Response<AddressModal> response) {
                            stopProgressHud(context);
                            if (response.code() == 200 && response.body() != null && response.body().get_id() != null) {
                                Utility.showSnackbar(context, context.getResources().getString(R.string.your_delivery_address_added_successfully));
                                dialog.dismiss();
                                getMyAddressess();
                            } else {
                                Log.e(TAG, response.toString());
                                Utility.ShowToastMessage(context, response.message());
                            }
                        }

                        @Override
                        public void onFailure(Call<AddressModal> call, Throwable t) {
                            stopProgressHud(context);
                            Log.e(TAG, t.getMessage(), t);
                        }
                    });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

    private void getMyAddressess() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            RetrofitClient.getAPIService().getMyAddressess(SharedPref.getSharedPreferences(context, Constants.TOKEN))
                    .enqueue(new Callback<GetAddressResponse>() {
                        @Override
                        public void onResponse(Call<GetAddressResponse> call, Response<GetAddressResponse> response) {
                            if (response.code() == 200 && response.body() != null && response.body().getAddresses() != null && response.body().getAddresses().size() != 0) {
                                addressArrayList = response.body().getAddresses();
                                addressListAdapter = new AddressListAdapter(context, addressArrayList, PaymentDetailsFragment.this);
                                addressesRv.setAdapter(addressListAdapter);
                                getDeliverySlots();
                            } else {
                                stopProgressHud(context);
                                Log.e(TAG, response.toString());
                                addressesRv.setAdapter(null);
                            }
                        }

                        @Override
                        public void onFailure(Call<GetAddressResponse> call, Throwable t) {
                            stopProgressHud(context);
                            Log.e(TAG, t.getMessage(), t);
                        }
                    });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

    private void getDeliverySlots() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            RetrofitClient.getAPIService().getDeliverySlots()
                    .enqueue(new Callback<DeliverySlotsResponse>() {
                        @Override
                        public void onResponse(Call<DeliverySlotsResponse> call, Response<DeliverySlotsResponse> response) {
                            if (response.code() == 200 && response.body() != null && response.body().getDeliverySlots() != null && response.body().getDeliverySlots().size() != 0) {
                                deliverySlotsArrayList = response.body().getDeliverySlots();
                                deliverSlotAdapter = new DeliverySlotAdapter(context, deliverySlotsArrayList, PaymentDetailsFragment.this, PaymentDetailsFragment.this);
                                deliverySlotsRv.setAdapter(deliverSlotAdapter);
                                getPaymentModes();
                            } else {
                                stopProgressHud(context);
                                Log.e(TAG, response.toString());
                                addressesRv.setAdapter(null);
                            }
                        }

                        @Override
                        public void onFailure(Call<DeliverySlotsResponse> call, Throwable t) {
                            stopProgressHud(context);
                            Log.e(TAG, t.getMessage(), t);
                        }
                    });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

    private void getPaymentModes() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            RetrofitClient.getAPIService().getPaymentModes()
                    .enqueue(new Callback<PaymentModesResponse>() {
                        @Override
                        public void onResponse(Call<PaymentModesResponse> call, Response<PaymentModesResponse> response) {
                            stopProgressHud(context);
                            if (response.code() == 200 && response.body() != null && response.body().getPaymentModes() != null && response.body().getPaymentModes().size() != 0) {
                                paymentModesArrayList = response.body().getPaymentModes();
                                paymentModesAdapter = new PaymentModesAdapter(context, paymentModesArrayList, PaymentDetailsFragment.this, PaymentDetailsFragment.this);
                                paymentModesRV.setAdapter(paymentModesAdapter);
                            } else {
                                Log.e(TAG, response.toString());
                                addressesRv.setAdapter(null);
                            }
                        }

                        @Override
                        public void onFailure(Call<PaymentModesResponse> call, Throwable t) {
                            stopProgressHud(context);
                            Log.e(TAG, t.getMessage(), t);
                        }
                    });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

    @Override
    public void onItemClicked(int position, View view, String value) {
        switch (view.getId()) {
            case R.id.ivBtnOptions:
                selectedAddressPosition = position;
                showAddressOptionsDialogue(view);
                break;

            case R.id.txvBtnDeliveryAddress:
                SharedPref.setSharedPreference(context, Constants.DELIVERY_ADDRESS_ID, addressArrayList.get(position).get_id());
                addressId = addressArrayList.get(position).get_id();
                Log.e(TAG, "onItemClicked addressId: " + addressId);
                addressListAdapter.notifyDataSetChanged();
                break;
        }

        switch (value) {
            case "R.id.rbDay":
                deliverySlotId = deliverySlotsArrayList.get(position).get_id();
                Log.e(TAG, "onItemClicked deliverySlotId: " + deliverySlotId);
                deliverSlotAdapter.notifyDataSetChanged();
                txvDeliverySlot.setText(date + ", " + deliverySlotsArrayList.get(position).getStart() + "-" + deliverySlotsArrayList.get(position).getEnd());

                break;

            case "R.id.rbPaymentMode":
                paymentModeId = paymentModesArrayList.get(position).get_id();
                Log.e(TAG, "onItemClicked paymentModeId: " + paymentModeId);
                paymentModesAdapter.notifyDataSetChanged();
                break;
        }
    }

    public void showAddressOptionsDialogue(View view) {
        addressOptionDialogue = new PopupMenu(context, view);
        addressOptionDialogue.setOnMenuItemClickListener(this);
        addressOptionDialogue.inflate(R.menu.address_options);
        addressOptionDialogue.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.optDeleteAddress:
                callDeleteAddressAPI(addressArrayList.get(selectedAddressPosition).get_id());
                break;
            case R.id.optCloseDialog:
                addressOptionDialogue.dismiss();
                break;
        }
        return false;
    }

    private void callDeleteAddressAPI(String addressId) {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            RetrofitClient.getAPIService().callDeleteAddressAPI(SharedPref.getSharedPreferences(context, Constants.TOKEN), addressId)
                    .enqueue(new Callback<SucessResult>() {
                        @Override
                        public void onResponse(Call<SucessResult> call, Response<SucessResult> response) {
                            if (response.code() == 200 && response.body() != null && response.body().getMessage() != null) {
                                Utility.showSnackbar(context, context.getResources().getString(R.string.address_deleted_succesfully));
                                addressOptionDialogue.dismiss();
                                getMyAddressess();
                            } else {
                                stopProgressHud(context);
                                Log.e(TAG, response.toString());
                                Utility.ShowToastMessage(context, response.message());
                            }
                        }

                        @Override
                        public void onFailure(Call<SucessResult> call, Throwable t) {
                            stopProgressHud(context);
                            Log.e(TAG, t.getMessage(), t);
                        }
                    });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

    private void callCreateOrderAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            CreateOrderRequest ob = new CreateOrderRequest();
            ob.setAddress(addressId);
            ob.setDeliverySlot(deliverySlotId);
            ob.setPaymentMode(paymentModeId);
            RetrofitClient.getAPIService().callCreateOrderAPI(SharedPref.getSharedPreferences(context, Constants.TOKEN), ob)
                    .enqueue(new Callback<SucessResult>() {
                        @Override
                        public void onResponse(Call<SucessResult> call, Response<SucessResult> response) {
                            stopProgressHud(context);
                            if (response.code() == 200 && response.body() != null && response.body().getMessage() != null) {
                                SharedPref.saveItemsArrayListInSharedPref(context, new ArrayList<>());
                                showOrderSuccessfullDialog();
                            } else {
                                stopProgressHud(context);
                                Log.e(TAG, response.toString());
                                Utility.showSnackbar(context, context.getResources().getString(R.string.cart_total_is_below_minimum_cart_value));
                            }
                        }

                        @Override
                        public void onFailure(Call<SucessResult> call, Throwable t) {
                            stopProgressHud(context);
                            Log.e(TAG, t.getMessage(), t);
                        }
                    });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

    public void checkPaymentConstrainst() {
        if (addressId == null || addressId.length() == 0)
            Utility.showSnackbar(context, context.getResources().getString(R.string.please_select_your_delivery_address));
        else if (deliverySlotId == null || deliverySlotId.length() == 0)
            Utility.showSnackbar(context, context.getResources().getString(R.string.please_select_your_delivery_schedule));
        else if (paymentModeId == null || paymentModeId.length() == 0)
            Utility.showSnackbar(context, context.getResources().getString(R.string.please_select_your_payment_options));

        else
            callCreateOrderAPI();

        Log.e(TAG, "onItemClicked addressId: " + addressId);
        Log.e(TAG, "onItemClicked deliverySlotId: " + deliverySlotId);
        Log.e(TAG, "onItemClicked paymentModeId: " + paymentModeId);
    }


    public void showOrderSuccessfullDialog() {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_order_succesfull);

        ((TextView) dialog.findViewById(R.id.txvButtonHomePage)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                context.startActivity(new Intent(context, HomePageActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK & Intent.FLAG_ACTIVITY_NEW_TASK));
                ((CheckOutActivity) getActivity()).finishAffinity();
            }
        });
        ((TextView) dialog.findViewById(R.id.txvButtonOrderHistory)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                context.startActivity(new Intent(context, HomePageActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK & Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra(Constants.FRAGMENT_FLAG, Constants.FRAGMENT_ORDER_LIST));
                ((CheckOutActivity) getActivity()).finishAffinity();
            }
        });
        dialog.show();

        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }
}
