package com.dataignyte.popolr.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.activities.OrderDetailsActivity;
import com.dataignyte.popolr.adapters.OrderListAdapter;
import com.dataignyte.popolr.helper.OnListItemClicked;
import com.dataignyte.popolr.helper.SharedPref;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.APICaller.RetrofitClient;
import com.dataignyte.popolr.retrofit.model.response.OrderListModel;
import com.dataignyte.popolr.retrofit.model.response.OrderListResponse;
import com.dataignyte.popolr.retrofit.model.response.OrdersHeader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrdersFragment extends BaseFragment implements OnListItemClicked {

    private static final String TAG = MyOrdersFragment.class.getSimpleName();
    private ExpandableListView expandableListView;
    private Context context;
    private ArrayList<OrderListModel> ordersArrayList = new ArrayList<>();
    private HashMap<String, ArrayList<OrderListModel>> dateWiseOrdersMap = new HashMap<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_orders, container, false);
        initialise(view);
        return view;
    }

    private void initialise(View view) {
        context = getActivity();
        expandableListView = (ExpandableListView) view.findViewById(R.id.expandableListView);
        getMyOrdersAPI();
    }

    private void getMyOrdersAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            RetrofitClient.getAPIService().getMyOrdersAPI(SharedPref.getSharedPreferences(context, Constants.TOKEN))
                    .enqueue(new Callback<OrderListResponse>() {
                        @Override
                        public void onResponse(Call<OrderListResponse> call, Response<OrderListResponse> response) {
                            stopProgressHud(context);
                            if (response.code() == 200 && response.body() != null && response.body().getOrders() != null) {
                                ordersArrayList = response.body().getOrders();

                                List<String> datesList = new ArrayList<>();
                                ArrayList<OrdersHeader> orderHeaderArrayList = new ArrayList<>();

                                for (int i = 0; i < ordersArrayList.size(); i++) {
                                    String date = Utility.convertDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "EEE, MMM dd yyyy", ordersArrayList.get(i).getOrderPlacedDate());
                                    if (!datesList.contains(date)) {
                                        datesList.add(date);
                                    }
                                }

                                for (int i = 0; i < datesList.size(); i++) {
                                    ArrayList<OrderListModel> dateWiseOrderList = new ArrayList<>();
                                    for (int j = 0; j < ordersArrayList.size(); j++) {
                                        String date = Utility.convertDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "EEE, MMM dd yyyy", ordersArrayList.get(j).getOrderPlacedDate());
                                        if (date.equalsIgnoreCase(datesList.get(i)))
                                            dateWiseOrderList.add(ordersArrayList.get(j));
                                    }
                                    dateWiseOrdersMap.put(datesList.get(i), dateWiseOrderList);
                                }
                                expandableListView.setAdapter(new OrderListAdapter(context, dateWiseOrdersMap, datesList, MyOrdersFragment.this));
                                expandableListView.expandGroup(0);
                            } else {
                                Log.e(TAG, response.toString());
                                Utility.ShowToastMessage(context, response.message());
                            }
                        }

                        @Override
                        public void onFailure(Call<OrderListResponse> call, Throwable t) {
                            stopProgressHud(context);
                            Log.e(TAG, t.getMessage(), t);
                        }
                    });
        } else {
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
        }
    }

    @Override
    public void onItemClicked(int position, View view, String value) {
        switch (view.getId()) {
            case R.id.cvButtonView:
                startActivity(new Intent(context, OrderDetailsActivity.class)
                        .putExtra(Constants.ORDER_ID, value));
                break;
        }
    }
}
