package com.dataignyte.popolr.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.dataignyte.popolr.R;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import com.dataignyte.popolr.adapters.AddressListAdapter;
import com.dataignyte.popolr.helper.*;
import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.retrofit.APICaller.RetrofitClient;
import com.dataignyte.popolr.retrofit.model.response.AddressModal;
import com.dataignyte.popolr.retrofit.model.response.CartResponse;
import com.dataignyte.popolr.retrofit.model.response.GetAddressResponse;
import com.dataignyte.popolr.retrofit.model.response.SucessResult;
import com.dataignyte.popolr.retrofit.model.response.UserDetailsResponse;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends BaseFragment implements View.OnClickListener, OnListItemClicked, PopupMenu.OnMenuItemClickListener {

    private static final String TAG = ProfileFragment.class.getSimpleName();
    private Context context;
    private ImageView ivProfileImage;
    private RecyclerView addressesRv;
    private TextView txvName, txvMobileNo, txvEmail;
    private File uploadfile;
    private int selectedPosition = -1;
    private PopupMenu addressOptionDialogue;
    private Dialog addAddressDialog;
    private ArrayList<AddressModal> addressArrayList;
    private AddressListAdapter addressListAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        initialise(view);
        return view;
    }

    private void initialise(View view) {
        context = getContext();
        ivProfileImage = (ImageView) view.findViewById(R.id.ivProfileImage);
        txvName = (TextView) view.findViewById(R.id.txvName);
        txvMobileNo = (TextView) view.findViewById(R.id.txvMobileNo);
        txvEmail = (TextView) view.findViewById(R.id.txvEmail);
        addressesRv = (RecyclerView) view.findViewById(R.id.addressesRv);
        addressesRv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

        ((TextView) view.findViewById(R.id.txvBtnAddAddress)).setOnClickListener(this);
        ((TextView) view.findViewById(R.id.txvBtnCHangeProfilePic)).setOnClickListener(this);
        ((ImageView) view.findViewById(R.id.ivBtnEdit)).setOnClickListener(this);
        ((CardView) view.findViewById(R.id.cvBtnChangePassword)).setOnClickListener(this);

        getUserDetails();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBtnEdit:
                break;

            case R.id.txvBtnCHangeProfilePic:
                if (ContextCompat.checkSelfPermission(context, CAMERA) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    final CharSequence[] options = {context.getString(R.string.from_camera), context.getString(R.string.from_gallery), context.getString(R.string.close)};
                    Utility.selectimag(context, this, context.getString(R.string.add_photo), options);
                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{CAMERA, READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, Constants.PermissionCodes.PERMISSION_REQUEST_CODE);
                }
                break;

            case R.id.txvBtnAddAddress:
                showAddAddressDialog();
                break;

            case R.id.cvBtnChangePassword:
                break;
        }
    }

    @Override
    public void onItemClicked(int position, View view, String value) {
        switch (view.getId()) {
            case R.id.ivBtnOptions:
                selectedPosition = position;
                showAddressOptionsDialogue(view);
                break;

            case R.id.txvBtnDeliveryAddress:
                SharedPref.setSharedPreference(context, Constants.DELIVERY_ADDRESS_ID, addressArrayList.get(position).get_id());
                addressListAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.PermissionCodes.PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    final CharSequence[] options = {context.getString(R.string.from_camera), context.getString(R.string.from_gallery), context.getString(R.string.close)};
                    Utility.selectimag(context, this, context.getString(R.string.add_photo), options);
                } else {
                    Utility.ShowToastMessage(context, "permission denied");
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            File file = null;
            String uploadfilePath ="";
            switch (requestCode) {
                case Constants.PermissionCodes.CAMERA__REQUEST_CODE:
                    uploadfilePath = Utility.imageFilePath;
                    file = new File(uploadfilePath);
                    uploadfile = file;
                    Glide.with(context).load(uploadfilePath).into(ivProfileImage);
                    break;

                case Constants.PermissionCodes.GALLERY_REQUEST_CODE:
                    Uri selectedImage = data.getData();
                    try {
                        uploadfilePath = Utility.getFilePath(context, selectedImage);
                        file = new File(uploadfilePath);
                        uploadfile = file;
                        Glide.with(context).load(selectedImage).into(ivProfileImage);
                    } catch (URISyntaxException e) {
                        Log.e(TAG, "onActivityResult: " + e.getMessage());
                    }
                    break;
            }
        }
    }

    public void showAddressOptionsDialogue(View view) {
        addressOptionDialogue = new PopupMenu(context, view);
        addressOptionDialogue.setOnMenuItemClickListener(this);
        addressOptionDialogue.inflate(R.menu.address_options);
        addressOptionDialogue.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.optDeleteAddress:
                callDeleteAddressAPI(addressArrayList.get(selectedPosition).get_id());
                break;
            case R.id.optCloseDialog:
                addressOptionDialogue.dismiss();
                break;
        }
        return false;
    }


    public void showAddAddressDialog(){
        addAddressDialog = new Dialog(context);
        addAddressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        addAddressDialog.setCancelable(false);
        addAddressDialog.setContentView(R.layout.dialog_add_address);

        EditText edtApartmentNo = (EditText) addAddressDialog.findViewById(R.id.edtApartmentNo);
        EditText edtLocalityName = (EditText) addAddressDialog.findViewById(R.id.edtLocalityName);
        EditText edtCityName = (EditText) addAddressDialog.findViewById(R.id.edtCityName);
        EditText edtStateName = (EditText) addAddressDialog.findViewById(R.id.edtStateName);
        EditText edtPincode = (EditText) addAddressDialog.findViewById(R.id.edtPincode);
        ((ImageView) addAddressDialog.findViewById(R.id.ivBtnClose)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAddressDialog.dismiss();
            }
        });

        ((TextView) addAddressDialog.findViewById(R.id.txvButtonAdd)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edtApartmentNo.getText().toString().trim().isEmpty())
                    Utility.ShowToastMessage(context, "Enter your apartment/flat/plot number");
                else if (edtLocalityName.getText().toString().trim().isEmpty())
                    Utility.ShowToastMessage(context, "Enter your locality name");
                else if (edtCityName.getText().toString().trim().isEmpty())
                    Utility.ShowToastMessage(context, "Enter your city name");
                else if (edtStateName.getText().toString().trim().isEmpty())
                    Utility.ShowToastMessage(context, "Enter your state name");
                else if (edtPincode.getText().toString().trim().isEmpty())
                    Utility.ShowToastMessage(context, "Enter your pincode");
                else {
                    AddressModal object = new AddressModal();
                    object.setLine1(edtApartmentNo.getText().toString().trim());
                    object.setLine2(edtLocalityName.getText().toString().trim());
                    object.setCity(edtCityName.getText().toString().trim());
                    object.setState(edtStateName.getText().toString().trim());
                    object.setPincode(edtPincode.getText().toString().trim());
                    callAddAddressAPI(object);
                }
            }
        });
        addAddressDialog.show();

        Window window = addAddressDialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private void getUserDetails() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            RetrofitClient.getAPIService().getUserDetails(SharedPref.getSharedPreferences(context, Constants.TOKEN))
                    .enqueue(new Callback<UserDetailsResponse>() {
                @Override
                public void onResponse(Call<UserDetailsResponse> call, Response<UserDetailsResponse> response) {
                    if (response.code() == 200 && response.body() != null && response.body().get_id() != null) {
                        txvName.setText(response.body().getFirstName() + " "+ response.body().getLastName());
                        txvEmail.setText(response.body().getEmail());
                        txvMobileNo.setText(String.valueOf(response.body().getPhoneNumber()));
                        getMyAddressess();
                    } else {
                        stopProgressHud(context);
                        Log.e(TAG, response.toString());
                        Utility.ShowToastMessage(context, response.message());
                    }

                }

                @Override
                public void onFailure(Call<UserDetailsResponse> call, Throwable t) {
                    stopProgressHud(context);
                    Log.e(TAG, t.getMessage(), t);
                }
            });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

    private void getMyAddressess() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            RetrofitClient.getAPIService().getMyAddressess(SharedPref.getSharedPreferences(context, Constants.TOKEN))
                    .enqueue(new Callback<GetAddressResponse>() {
                @Override
                public void onResponse(Call<GetAddressResponse> call, Response<GetAddressResponse> response) {
                    stopProgressHud(context);
                    if (response.code() == 200 && response.body() != null && response.body().getAddresses() != null && response.body().getAddresses().size()!=0) {
                        addressArrayList = response.body().getAddresses();
                        addressListAdapter = new AddressListAdapter(context, addressArrayList, ProfileFragment.this);
                        addressesRv.setAdapter(addressListAdapter);
                    } else {
                        Log.e(TAG, response.toString());
                        addressesRv.setAdapter(null);
                    }
                }

                @Override
                public void onFailure(Call<GetAddressResponse> call, Throwable t) {
                    stopProgressHud(context);
                    Log.e(TAG, t.getMessage(), t);
                }
            });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

    private void callAddAddressAPI(AddressModal requestObject) {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            RetrofitClient.getAPIService().callAddAddressAPI(SharedPref.getSharedPreferences(context, Constants.TOKEN), requestObject)
                    .enqueue(new Callback<AddressModal>() {
                        @Override
                        public void onResponse(Call<AddressModal> call, Response<AddressModal> response) {
                            stopProgressHud(context);
                            if (response.code() == 200 && response.body() != null && response.body().get_id() != null) {
                                Utility.ShowToastMessage(context, "Your delivery address added succesfully!");
                                addAddressDialog.dismiss();
                                getMyAddressess();
                            } else {
                                Log.e(TAG, response.toString());
                                Utility.ShowToastMessage(context, response.message());
                            }
                        }

                        @Override
                        public void onFailure(Call<AddressModal> call, Throwable t) {
                            stopProgressHud(context);
                            Log.e(TAG, t.getMessage(), t);
                        }
                    });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

    private void callDeleteAddressAPI(String addressId) {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud(context);
            RetrofitClient.getAPIService().callDeleteAddressAPI(SharedPref.getSharedPreferences(context, Constants.TOKEN), addressId)
                    .enqueue(new Callback<SucessResult>() {
                        @Override
                        public void onResponse(Call<SucessResult> call, Response<SucessResult> response) {
                            if (response.code() == 200 && response.body() != null && response.body().getMessage() != null) {
                                Utility.ShowToastMessage(context, getString(R.string.address_deleted_succesfully));
                                addressOptionDialogue.dismiss();
                                getMyAddressess();
                            } else {
                                stopProgressHud(context);
                                Log.e(TAG, response.toString());
                                Utility.ShowToastMessage(context, response.message());
                            }
                        }

                        @Override
                        public void onFailure(Call<SucessResult> call, Throwable t) {
                            stopProgressHud(context);
                            Log.e(TAG, t.getMessage(), t);
                        }
                    });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }
}
