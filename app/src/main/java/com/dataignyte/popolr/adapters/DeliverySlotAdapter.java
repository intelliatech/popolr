package com.dataignyte.popolr.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.dataignyte.popolr.R;
import com.dataignyte.popolr.fragments.PaymentDetailsFragment;
import com.dataignyte.popolr.helper.OnListItemClicked;
import com.dataignyte.popolr.retrofit.model.response.DeliverySlotModal;

import java.util.ArrayList;

public class DeliverySlotAdapter extends RecyclerView.Adapter<DeliverySlotAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<DeliverySlotModal> deliverySlotsArrayList;
    private OnListItemClicked onListItemClicked;
    private Fragment fragment;

    public DeliverySlotAdapter(Context context, ArrayList<DeliverySlotModal> deliverySlotsArrayList, OnListItemClicked onListItemClicked, Fragment fragment) {
        this.context = context;
        this.deliverySlotsArrayList = deliverySlotsArrayList;
        this.onListItemClicked = onListItemClicked;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public DeliverySlotAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.row_delivery_time_slot, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DeliverySlotAdapter.MyViewHolder holder, int position) {
        holder.rbDay.setText(deliverySlotsArrayList.get(position).getName());
        holder.txvStartTime.setText(deliverySlotsArrayList.get(position).getStart());
        holder.txvEndTime.setText(" -   " + deliverySlotsArrayList.get(position).getEnd());

        if (fragment != null && fragment instanceof PaymentDetailsFragment) {
            holder.rbDay.setChecked(((PaymentDetailsFragment) fragment).deliverySlotId.equalsIgnoreCase(deliverySlotsArrayList.get(position).get_id()));
        } else
            holder.rbDay.setChecked(true);

        holder.rbDay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    onListItemClicked.onItemClicked(position, buttonView.getRootView(), "R.id.rbDay");
                } else if (fragment != null && fragment instanceof PaymentDetailsFragment) {
                    ((PaymentDetailsFragment) fragment).deliverySlotId = "";
                    ((PaymentDetailsFragment) fragment).setDate();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return deliverySlotsArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CheckBox rbDay;
        TextView txvStartTime, txvEndTime;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            rbDay = (CheckBox) itemView.findViewById(R.id.rbDay);
            txvStartTime = (TextView) itemView.findViewById(R.id.txvStartTime);
            txvEndTime = (TextView) itemView.findViewById(R.id.txvEndTime);
        }
    }
}
