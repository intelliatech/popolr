package com.dataignyte.popolr.adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.helper.OnListItemClicked;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.model.response.OrderListModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrderListAdapter extends BaseExpandableListAdapter {
    private Context context;
    private OnListItemClicked onListItemClicked;
    private HashMap<String, ArrayList<OrderListModel>> dateOrderMap;
    private List<String> datesList;

    public OrderListAdapter(Context context, HashMap<String, ArrayList<OrderListModel>> dateOrderMap, List<String> datesList, OnListItemClicked onListItemClicked) {
        this.context = context;
        this.dateOrderMap = dateOrderMap;
        this.onListItemClicked = onListItemClicked;
        this.datesList = datesList;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return dateOrderMap.get(datesList.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        OrderListModel order = (OrderListModel) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.row_order_item, null);
        }

        ((TextView) convertView.findViewById(R.id.txvOrderStatus)).setText(order.getStatus());
        ((TextView) convertView.findViewById(R.id.txvOrderId)).setText(order.get_id());
        ((TextView) convertView.findViewById(R.id.txvItemCount)).setText(order.getNumberOfItems());
        ((TextView) convertView.findViewById(R.id.txvAmount)).setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfDouble(order.getTotal()));

        ((CardView) convertView.findViewById(R.id.cvButtonView)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onListItemClicked.onItemClicked(childPosition, v, order.get_id());
            }
        });
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return dateOrderMap.get(datesList.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return datesList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return datesList.size();
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.row_order_header, null);
        }
        ((TextView) convertView.findViewById(R.id.txvDate)).setText(listTitle);

        if (getChildrenCount(groupPosition) == 0) {
            ((ImageView) convertView.findViewById(R.id.indicator)).setVisibility(View.INVISIBLE);
        } else {
            ((ImageView) convertView.findViewById(R.id.indicator)).setVisibility(View.VISIBLE);
            ((ImageView) convertView.findViewById(R.id.indicator)).setImageResource(isExpanded ? R.drawable.ic_expand_less : R.drawable.ic_expand_more);
        }


        return convertView;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }


    @Override
    public boolean hasStableIds() {
        return false;
    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }


    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {

    }

    @Override
    public void onGroupCollapsed(int groupPosition) {

    }

    @Override
    public long getCombinedChildId(long groupId, long childId) {
        return 0;
    }

    @Override
    public long getCombinedGroupId(long groupId) {
        return 0;
    }
}
