package com.dataignyte.popolr.adapters;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.helper.OnListItemClicked;
import com.dataignyte.popolr.helper.SharedPref;
import com.dataignyte.popolr.retrofit.model.response.AddressModal;

import java.util.ArrayList;

public class AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.MyViewHolder> {
    private Context context;
    private OnListItemClicked onListItemClicked;
    private ArrayList<AddressModal> addressArrayList;

    public AddressListAdapter(Context context, ArrayList<AddressModal> addressArrayList, OnListItemClicked onListItemClicked) {
        this.context = context;
        this.onListItemClicked = onListItemClicked;
        this.addressArrayList = addressArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.row_address_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.txvAddress.setText(addressArrayList.get(position).getLine1() + "\n" +
                addressArrayList.get(position).getLine2() + "\n" +
                addressArrayList.get(position).getCity() + ",\t" + addressArrayList.get(position).getState()
                + "\nPin: " + addressArrayList.get(position).getPincode());

        if (SharedPref.getSharedPreferences(context, Constants.DELIVERY_ADDRESS_ID) != null
                && SharedPref.getSharedPreferences(context, Constants.DELIVERY_ADDRESS_ID).equalsIgnoreCase(addressArrayList.get(position).get_id())) {
            holder.txvBtnDeliveryAddress.setTextColor(context.getColor(R.color.colorPrimaryDark));
            holder.ivIsDeliveryAddress.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
        } else {
            holder.txvBtnDeliveryAddress.setTextColor(context.getColor(R.color.textGrey));
            holder.ivIsDeliveryAddress.setColorFilter(ContextCompat.getColor(context, R.color.textGrey), android.graphics.PorterDuff.Mode.SRC_IN);
        }

        holder.txvBtnDeliveryAddress.setOnClickListener(new MyClickListener(holder, position));

        holder.ivBtnOptions.setOnClickListener(new MyClickListener(holder, position));
    }

    @Override
    public int getItemCount() {
        if (addressArrayList != null)
            return addressArrayList.size();
        else
            return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txvAddress, txvBtnDeliveryAddress;
        ImageView ivBtnOptions, ivIsDeliveryAddress;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txvAddress = (TextView) itemView.findViewById(R.id.txvAddress);
            txvBtnDeliveryAddress = (TextView) itemView.findViewById(R.id.txvBtnDeliveryAddress);
            ivBtnOptions = (ImageView) itemView.findViewById(R.id.ivBtnOptions);
            ivIsDeliveryAddress = (ImageView) itemView.findViewById(R.id.ivIsDeliveryAddress);
        }
    }

    private class MyClickListener implements View.OnClickListener {
        private MyViewHolder holder;
        private int position;

        public MyClickListener(MyViewHolder holder, int position) {
            this.holder = holder;
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivBtnOptions:
                case R.id.txvBtnDeliveryAddress:
                    onListItemClicked.onItemClicked(position, v, "");
                    break;
            }
        }
    }
}
