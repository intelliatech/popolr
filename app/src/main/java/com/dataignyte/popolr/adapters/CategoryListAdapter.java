package com.dataignyte.popolr.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dataignyte.popolr.R;
import com.dataignyte.popolr.helper.OnListItemClicked;
import com.dataignyte.popolr.retrofit.model.response.Category;

import java.util.ArrayList;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.MyViewHolder> {
    private Context context;
    public int selectedPosition = 0;
    private ArrayList<Category> arrayListCategory = new ArrayList<>();
    private OnListItemClicked onListItemClicked;

    public CategoryListAdapter(Context context, ArrayList<Category> arrayListCategory, OnListItemClicked onListItemClicked) {
        this.context = context;
        this.arrayListCategory = arrayListCategory;
        this.onListItemClicked = onListItemClicked;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.row_categories, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (position == selectedPosition) {
            holder.txvCategoryName.setTypeface(null, Typeface.BOLD);
            holder.txvCategoryName.setTextSize(18.0f);
            holder.txvCategoryName.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        } else {
            holder.txvCategoryName.setTypeface(null, Typeface.NORMAL);
            holder.txvCategoryName.setTextSize(16.0f);
            holder.txvCategoryName.setTextColor(Color.GRAY);
        }

        holder.txvCategoryName.setText(arrayListCategory.get(position).getName());

        holder.rlBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onListItemClicked.onItemClicked(position,v,"");
            }
        });


    }

    @Override
    public int getItemCount() {
        return arrayListCategory.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout rlBtn;
        public TextView txvCategoryName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            rlBtn = (RelativeLayout) itemView.findViewById(R.id.rlBtnCategory);
            txvCategoryName = (TextView) itemView.findViewById(R.id.txvCategoryName);
        }
    }
}
