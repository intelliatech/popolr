package com.dataignyte.popolr.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.model.response.OrderItem;

import java.util.ArrayList;

public class OrderDetailItemsAdapter extends RecyclerView.Adapter<OrderDetailItemsAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<OrderItem> itemsArrayList;

    public OrderDetailItemsAdapter(Context context, ArrayList<OrderItem> itemsArrayList) {
        this.context = context;
        this.itemsArrayList = itemsArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.row_order_summary_order_details, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.txvItemName.setText(itemsArrayList.get(position).getItem().getName());
        holder.txvItemQuantity.setText(String.valueOf(itemsArrayList.get(position).getQuantity()));
        holder.txvItemPrice.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfFloat(itemsArrayList.get(position).getTotal()));
    }

    @Override
    public int getItemCount() {
        return itemsArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txvItemName, txvItemQuantity, txvItemPrice;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txvItemName = itemView.findViewById(R.id.txvItemName);
            txvItemQuantity = itemView.findViewById(R.id.txvItemQuantity);
            txvItemPrice = itemView.findViewById(R.id.txvItemPrice);
        }
    }
}
