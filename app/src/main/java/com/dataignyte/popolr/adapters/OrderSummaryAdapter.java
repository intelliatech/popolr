package com.dataignyte.popolr.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.model.response.ItemDetails;

import java.util.ArrayList;

public class OrderSummaryAdapter extends RecyclerView.Adapter<OrderSummaryAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<ItemDetails> itemsArrayList;

    public OrderSummaryAdapter(Context context, ArrayList<ItemDetails> itemsArrayList) {
        this.context = context;
        this.itemsArrayList = itemsArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.row_order_summary, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.txvItemName.setText(itemsArrayList.get(position).getName());
        holder.txvItemQuantity.setText(String.valueOf(itemsArrayList.get(position).getQuantity()));
        holder.txvItemPrice.setText(Constants.RUPEE +" "+ Utility.get2DecimalStringOfDouble(itemsArrayList.get(position).getQuantity() *
                itemsArrayList.get(position).getSellPrice()));
    }

    @Override
    public int getItemCount() {
        return itemsArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txvItemName, txvItemQuantity, txvItemPrice;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txvItemName = itemView.findViewById(R.id.txvItemName);
            txvItemQuantity = itemView.findViewById(R.id.txvItemQuantity);
            txvItemPrice = itemView.findViewById(R.id.txvItemPrice);
        }
    }
}
