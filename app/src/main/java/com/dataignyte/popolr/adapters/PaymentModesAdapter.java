package com.dataignyte.popolr.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.dataignyte.popolr.R;
import com.dataignyte.popolr.fragments.PaymentDetailsFragment;
import com.dataignyte.popolr.helper.OnListItemClicked;
import com.dataignyte.popolr.retrofit.model.PaymentModeModel;

import java.util.ArrayList;

public class PaymentModesAdapter extends RecyclerView.Adapter<PaymentModesAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<PaymentModeModel> paymentModesArrayList;
    private OnListItemClicked onListItemClicked;
    private Fragment fragment;

    public PaymentModesAdapter(Context context, ArrayList<PaymentModeModel> paymentModesArrayList, OnListItemClicked onListItemClicked, Fragment fragment) {
        this.context = context;
        this.paymentModesArrayList = paymentModesArrayList;
        this.onListItemClicked = onListItemClicked;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.row_payment_mode, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.rbPaymentMode.setText(paymentModesArrayList.get(position).getName());

        if (fragment != null && fragment instanceof PaymentDetailsFragment)
            holder.rbPaymentMode.setChecked(((PaymentDetailsFragment) fragment).paymentModeId.equalsIgnoreCase(paymentModesArrayList.get(position).get_id()));
        else
            holder.rbPaymentMode.setChecked(false);

        holder.rbPaymentMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    onListItemClicked.onItemClicked(position, buttonView.getRootView(), "R.id.rbPaymentMode");
                else if (fragment != null && fragment instanceof PaymentDetailsFragment)
                    ((PaymentDetailsFragment) fragment).paymentModeId = "";

            }
        });

    }

    @Override
    public int getItemCount() {
        return paymentModesArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CheckBox rbPaymentMode;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            rbPaymentMode = (CheckBox) itemView.findViewById(R.id.rbPaymentMode);
        }
    }
}
