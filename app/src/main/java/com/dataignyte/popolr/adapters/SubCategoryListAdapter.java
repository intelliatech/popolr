package com.dataignyte.popolr.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.activities.ItemsListActivity;
import com.dataignyte.popolr.helper.OnListItemClicked;
import com.dataignyte.popolr.retrofit.model.response.SubCategory;

import java.util.ArrayList;

public class SubCategoryListAdapter extends RecyclerView.Adapter<SubCategoryListAdapter.MyViewHolder> {

    private String TAG = SubCategoryListAdapter.class.getSimpleName();
    private Context context;
    private ArrayList<SubCategory> subCategoryArrayList;
    private OnListItemClicked onListItemClicked;

    public SubCategoryListAdapter(Context context, ArrayList<SubCategory> subCategoryArrayList, OnListItemClicked onListItemClicked) {
        this.context = context;
        this.subCategoryArrayList = subCategoryArrayList;
        this.onListItemClicked = onListItemClicked;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.row_sub_categories, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.txvSubCategoryName.setText(subCategoryArrayList.get(position).getName());

        if (subCategoryArrayList.get(position).getImages()!=null && subCategoryArrayList.get(position).getImages().size()!=0)
            Glide.with(context).load(Constants.URL.BASE_URL + subCategoryArrayList.get(position).getImages().get(0).replaceAll(" ", "%20")).into(holder.ivSubCategoryImage);

        holder.cvBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ItemsListActivity.class)
                        .putExtra(Constants.SUB_CATEGORY_ID, subCategoryArrayList.get(position).getId())
                        .putExtra(Constants.SUB_CATEGORY_NAME, subCategoryArrayList.get(position).getName()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return subCategoryArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CardView cvBtn;
        public ImageView ivSubCategoryImage;
        public TextView txvSubCategoryName;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            cvBtn = (CardView) itemView.findViewById(R.id.cvBtn);
            ivSubCategoryImage = (ImageView) itemView.findViewById(R.id.ivSubCategoryImage);
            txvSubCategoryName = (TextView) itemView.findViewById(R.id.txvSubCategoryName);
        }
    }
}
