package com.dataignyte.popolr.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.helper.OnListItemClicked;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.model.response.ItemDetails;

import java.util.ArrayList;

public class CartItemsAdapter extends RecyclerView.Adapter<CartItemsAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<ItemDetails> arrayListItems;
    private OnListItemClicked onListItemClicked;

    public CartItemsAdapter(Context context, ArrayList<ItemDetails> arrayListItems, OnListItemClicked onListItemClicked) {
        this.context = context;
        this.arrayListItems = arrayListItems;
        this.onListItemClicked = onListItemClicked;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.row_cart_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.txvItemName.setText(arrayListItems.get(position).getName());
        holder.txvItemCount.setText(String.valueOf( arrayListItems.get(position).getQuantity()));
        holder.txvPricePerItem.setText(Constants.RUPEE +" "+arrayListItems.get(position).getSellPrice());
        holder.txvTotalPrice.setText(Constants.RUPEE +" "+ Utility.get2DecimalStringOfDouble(arrayListItems.get(position).getQuantity() *
                arrayListItems.get(position).getSellPrice()));
//        holder.txvItemPrice.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfFloat(itemsArrayList.get(position).getSellPrice()));


        if (arrayListItems.get(position).getImages().size() != 0)
            Glide.with(context).load(Constants.URL.BASE_URL + arrayListItems.get(position).getImages().get(0).replaceAll(" ", "%20")).into(holder.ivItemImage);

        holder.cvBtnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onListItemClicked.onItemClicked(position,view,"");
            }
        });
        holder.cvBtnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onListItemClicked.onItemClicked(position,view,"");
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayListItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView ivItemImage;
        TextView txvItemName, txvPricePerItem, txvItemCount, txvTotalPrice;
        CardView cvBtnMinus, cvBtnPlus;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ivItemImage = (ImageView) itemView.findViewById(R.id.ivItemImage);
            txvItemName = (TextView) itemView.findViewById(R.id.txvItemName);
            txvPricePerItem = (TextView) itemView.findViewById(R.id.txvPricePerItem);
            txvItemCount = (TextView) itemView.findViewById(R.id.txvItemCount);
            txvTotalPrice = (TextView) itemView.findViewById(R.id.txvTotalPrice);
            cvBtnMinus = (CardView) itemView.findViewById(R.id.cvBtnMinus);
            cvBtnPlus = (CardView) itemView.findViewById(R.id.cvBtnPlus);
        }
    }
}
