package com.dataignyte.popolr.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.helper.OnListItemClicked;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.model.response.Department;

import java.util.ArrayList;

public class DepartmentsListAdapter extends RecyclerView.Adapter<DepartmentsListAdapter.MyViewHolder> implements Filterable {
    private ArrayList<Department> arrayListDepartments;
    private OnListItemClicked onListItemClicked;
    private Context context;
    private ArrayList<Department> finalArrayList;
    private RecordFilter recordFilter;

    public DepartmentsListAdapter(Context context, ArrayList<Department> arrayListDepartments, OnListItemClicked onListItemClicked) {
        this.context = context;
        this.arrayListDepartments = arrayListDepartments;
        this.finalArrayList = arrayListDepartments;
        this.onListItemClicked = onListItemClicked;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.row_departments, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.txvDepartmentName.setText(arrayListDepartments.get(position).getName());

        if (arrayListDepartments.get(position).getImages().size() != 0)
            Glide.with(context).load(Constants.URL.BASE_URL + arrayListDepartments.get(position).getImages().get(0).replaceAll(" ", "%20"))
                    .into(holder.ivDepartmentImage);

        holder.cvBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onListItemClicked.onItemClicked(position, v, "");
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayListDepartments.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    //
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        if (recordFilter==null)
            recordFilter = new RecordFilter();

        return recordFilter;
    }

    private class RecordFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint.length()!=0) {
                final ArrayList<Department> tempFilterList = new ArrayList<Department>(finalArrayList.size());
                for (int i = 0; i < finalArrayList.size(); i++) {
                    if (Utility.geNonNullString(finalArrayList.get(i).getName()).toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempFilterList.add(finalArrayList.get(i));
                    } else if (Utility.geNonNullString(finalArrayList.get(i).getId()).toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempFilterList.add(finalArrayList.get(i));
                    }
                }
                results.values = tempFilterList;
                results.count = tempFilterList.size();
                if (tempFilterList.size()==0) {
                    Utility.ShowToastMessage(context, context.getString(R.string.no_such_item_exist));
                }
            } else {
                results.values = finalArrayList;
                results.count = finalArrayList.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint,FilterResults results) {
            arrayListDepartments = (ArrayList<Department>) results.values;
            notifyDataSetChanged();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CardView cvBtn;
        public TextView txvDepartmentName;
        public ImageView ivDepartmentImage;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            cvBtn = (CardView) itemView.findViewById(R.id.cvBtn);
            txvDepartmentName = (TextView) itemView.findViewById(R.id.txvDepartmentName);
            ivDepartmentImage = (ImageView) itemView.findViewById(R.id.ivDepartmentImage);
        }
    }
}
