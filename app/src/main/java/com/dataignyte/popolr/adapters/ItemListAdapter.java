package com.dataignyte.popolr.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.activities.ItemDetailActivity;
import com.dataignyte.popolr.helper.OnListItemClicked;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.model.response.ItemDetails;

import java.util.ArrayList;

public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.MyViewHolder> implements Filterable {
    private Context context;
    private ArrayList<ItemDetails> itemsArrayList;
    private ArrayList<ItemDetails> finalArrayList;
    private RecordFilter recordFilter;
    private OnListItemClicked onListItemClicked;

    public ItemListAdapter(Context context, ArrayList<ItemDetails> itemsArrayList, OnListItemClicked onListItemClicked) {
        this.context = context;
        this.itemsArrayList = itemsArrayList;
        this.finalArrayList = itemsArrayList;
        this.onListItemClicked = onListItemClicked;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.row_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if (itemsArrayList.get(position).getQuantity() == 0) {
            holder.txvBtnAddToCart.setVisibility(View.VISIBLE);
            holder.llChangeCount.setVisibility(View.GONE);
        } else {
            holder.txvBtnAddToCart.setVisibility(View.GONE);
            holder.llChangeCount.setVisibility(View.VISIBLE);
        }
        holder.txvItemName.setText(itemsArrayList.get(position).getName());
        holder.txvItemPrice.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfFloat(itemsArrayList.get(position).getSellPrice()));

        holder.txvItemCount.setText(itemsArrayList.get(position).getQuantity() + "");

        if (itemsArrayList.get(position).getImages().size() != 0)
            Glide.with(context).load(Constants.URL.BASE_URL + itemsArrayList.get(position).getImages().get(0).replaceAll(" ", "%20")).into(holder.ivItemImage);

        holder.ivButtonLike.setOnClickListener(new MyClickListener(holder, position));
        holder.cvBtnMinus.setOnClickListener(new MyClickListener(holder, position));
        holder.cvBtnPlus.setOnClickListener(new MyClickListener(holder, position));
        holder.cvBtn.setOnClickListener(new MyClickListener(holder, position));
        holder.txvBtnAddToCart.setOnClickListener(new MyClickListener(holder, position));
    }

    @Override
    public int getItemCount() {
        return itemsArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        if (recordFilter == null)
            recordFilter = new RecordFilter();
        return recordFilter;
    }

    private class RecordFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint.length() != 0) {
                final ArrayList<ItemDetails> tempFilterList = new ArrayList<ItemDetails>(finalArrayList.size());
                for (int i = 0; i < finalArrayList.size(); i++) {
                    if (Utility.geNonNullString(finalArrayList.get(i).getName()).toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempFilterList.add(finalArrayList.get(i));
                    } else if (Utility.geNonNullString(finalArrayList.get(i).getDescription()).toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempFilterList.add(finalArrayList.get(i));
                    } else if (Utility.geNonNullString(finalArrayList.get(i).getSku()).toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempFilterList.add(finalArrayList.get(i));
                    } /*else if (Utility.geNonNullString(finalArrayList.get(i).getSubCategory().getName()).toLowerCase().contains(constraint.toString().toLowerCase())) {
                            tempFilterList.add(finalArrayList.get(i));
                        } else if (Utility.geNonNullString(finalArrayList.get(i).getSubCategory().getName()).toLowerCase().contains(constraint.toString().toLowerCase())) {
                            tempFilterList.add(finalArrayList.get(i));
                        } else if (Utility.geNonNullString(finalArrayList.get(i).getSubCategory().getCategory().getName()).toLowerCase().contains(constraint.toString().toLowerCase())) {
                            tempFilterList.add(finalArrayList.get(i));
                        } else if (Utility.geNonNullString(finalArrayList.get(i).getSubCategory().getCategory().getDepartment().getName()).toLowerCase().contains(constraint.toString().toLowerCase())) {
                            tempFilterList.add(finalArrayList.get(i));
                        } else if (Utility.geNonNullString(finalArrayList.get(i).getBrand().getName()).toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempFilterList.add(finalArrayList.get(i));
                    } */
                }
                results.values = tempFilterList;
                results.count = tempFilterList.size();
                if (tempFilterList.size() == 0) {
                    Utility.ShowToastMessage(context, context.getString(R.string.no_such_item_exist));
                }
            } else {
                results.values = finalArrayList;
                results.count = finalArrayList.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            itemsArrayList = (ArrayList<ItemDetails>) results.values;
            notifyDataSetChanged();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CardView cvBtn, cvBtnPlus, cvBtnMinus;
        public TextView txvItemName, txvItemCount, txvItemPrice, txvBtnAddToCart;
        public ImageView ivItemImage, ivButtonLike;
        public LinearLayout llChangeCount;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            cvBtn = (CardView) itemView.findViewById(R.id.cvBtn);
            cvBtnPlus = (CardView) itemView.findViewById(R.id.cvBtnPlus);
            cvBtnMinus = (CardView) itemView.findViewById(R.id.cvBtnMinus);
            ivItemImage = (ImageView) itemView.findViewById(R.id.ivItemImage);
            ivButtonLike = (ImageView) itemView.findViewById(R.id.ivButtonLike);
            txvItemName = (TextView) itemView.findViewById(R.id.txvItemName);
            txvItemCount = (TextView) itemView.findViewById(R.id.txvItemCount);
            txvItemPrice = (TextView) itemView.findViewById(R.id.txvItemPrice);
            txvBtnAddToCart = (TextView) itemView.findViewById(R.id.txvBtnAddToCart);
            llChangeCount = (LinearLayout) itemView.findViewById(R.id.llChangeCount);
        }
    }

    private class MyClickListener implements View.OnClickListener {
        private MyViewHolder holder;
        private int position;

        public MyClickListener(MyViewHolder holder, int position) {
            this.holder = holder;
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.cvBtnMinus:
                    onListItemClicked.onItemClicked(position, v, "");
                    break;
                case R.id.cvBtnPlus:
                    onListItemClicked.onItemClicked(position, v, "");
                    break;
                case R.id.ivButtonLike:
                    holder.ivButtonLike.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_like));
                    break;
                case R.id.txvBtnAddToCart:
                    onListItemClicked.onItemClicked(position, v, "");
                    break;
                case R.id.cvBtn:
                    /*Temp Disable*/
//                    context.startActivity(new Intent(context, ItemDetailActivity.class)
//                            .putExtra(Constants.ITEM_ID, itemsArrayList.get(position).getId()));
                    break;

            }
        }
    }
}
