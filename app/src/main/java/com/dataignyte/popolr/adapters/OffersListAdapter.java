package com.dataignyte.popolr.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.model.response.ItemDetails;
import com.dataignyte.popolr.retrofit.model.response.ItemsList;

import java.util.ArrayList;

public class OffersListAdapter extends RecyclerView.Adapter <OffersListAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<ItemDetails> itemsArrayList;

    public OffersListAdapter(Context context, ArrayList<ItemDetails> itemsArrayList) {
        this.context = context;
        this.itemsArrayList = itemsArrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.row_offers, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.txvOfferDescription.setText(itemsArrayList.get(position).getName());
        if (itemsArrayList.get(position).getImages()!=null && itemsArrayList.get(position).getImages().size()!=0)
            Glide.with(context).load(Constants.URL.BASE_URL + itemsArrayList.get(position).getImages().get(0).replaceAll(" ", "%20")).into(holder.ivOffer);

        holder.txvSellingPrice.setText(Constants.RUPEE +" "+ Utility.get2DecimalStringOfDouble(itemsArrayList.get(position).getSellPrice()));
        holder.txvMrp.setText(Constants.RUPEE +" "+ Utility.get2DecimalStringOfDouble(itemsArrayList.get(position).getMrp()));
        holder.txvMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txvDiscountPercent.setText(Utility.get2DecimalStringOfDouble(itemsArrayList.get(position).getDiscountPercent())+"% OFF");
    }

    @Override
    public int getItemCount() {
        return itemsArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class MyViewHolder  extends RecyclerView.ViewHolder {
        public TextView txvSellingPrice, txvMrp, txvDiscountPercent, txvOfferDescription, txvButtonViewOffer;
        public ImageView ivOffer;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ivOffer = (ImageView) itemView.findViewById(R.id.ivOffer);
            txvSellingPrice = (TextView) itemView.findViewById(R.id.txvSellingPrice);
            txvMrp = (TextView) itemView.findViewById(R.id.txvMrp);
            txvDiscountPercent = (TextView) itemView.findViewById(R.id.txvDiscountPercent);

            txvOfferDescription = (TextView) itemView.findViewById(R.id.txvOfferDescription);
            txvButtonViewOffer =  (TextView) itemView.findViewById(R.id.txvButtonViewOffer);
        }
    }
}
