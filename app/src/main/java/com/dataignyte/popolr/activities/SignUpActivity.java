package com.dataignyte.popolr.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.helper.SharedPref;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.APICaller.RetrofitClient;
import com.dataignyte.popolr.retrofit.model.request.SignUpRequest;
import com.dataignyte.popolr.retrofit.model.response.UserDetail;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = SignUpActivity.class.getSimpleName();
    private Context context;
    private EditText edtFirstName, edtLastName, edtNumber, edtEmail, edtPassword, edtConfirmPassword;
    private Spinner spnGender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initialise();
    }

    private void initialise() {
        context = this;
        edtFirstName = (EditText) findViewById(R.id.edtFirstName);
        edtLastName = (EditText) findViewById(R.id.edtLastName);
        spnGender = (Spinner) findViewById(R.id.spnGender);
        edtNumber = (EditText) findViewById(R.id.edtNumber);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        edtConfirmPassword = (EditText) findViewById(R.id.edtConfirmPassword);

        ((TextView) findViewById(R.id.txvButtonSignup)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.llBtnLogin)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txvButtonSignup:
                if (edtFirstName.getText().toString().trim().isEmpty()) {
                    Utility.ShowToastMessage(context, getString(R.string.enter_your_first_name));
                    edtFirstName.setError(getString(R.string.enter_your_first_name));
                } else if (edtLastName.getText().toString().trim().isEmpty()) {
                    Utility.ShowToastMessage(context, getString(R.string.enter_your_last_name));
                    edtLastName.setError(getString(R.string.enter_your_last_name));
                } else if (spnGender.getSelectedItemPosition()==0) {
                    Utility.ShowToastMessage(context, getString(R.string.select_gender));
                } else if (edtNumber.getText().toString().trim().isEmpty()) {
                    Utility.ShowToastMessage(context, getString(R.string.enter_your_number));
                    edtNumber.setError(getString(R.string.enter_your_number));
                } else if (edtNumber.getText().toString().trim().length()<10) {
                    Utility.ShowToastMessage(context, getString(R.string.enter_valid_number));
                    edtNumber.setError(getString(R.string.enter_valid_number));
                } else if (edtEmail.getText().toString().trim().isEmpty()) {
                    Utility.ShowToastMessage(context, getString(R.string.enter_email_adress));
                    edtEmail.setError(getString(R.string.enter_email_adress));
                } else if (!edtEmail.getText().toString().trim().isEmpty() && !Patterns.EMAIL_ADDRESS.matcher(edtEmail.getText().toString().trim()).matches()) {
                    Utility.ShowToastMessage(context, getString(R.string.enter_valid_email));
                    edtEmail.setError(getString(R.string.enter_valid_email));
                } else if (edtPassword.getText().toString().trim().isEmpty()) {
                    Utility.ShowToastMessage(context, getString(R.string.enter_password));
                } else if (edtPassword.getText().toString().trim().length()<5) {
                    Utility.ShowToastMessage(context, getString(R.string.enter_valid_password));
                } else if (edtConfirmPassword.getText().toString().trim().isEmpty()) {
                    Utility.ShowToastMessage(context, getString(R.string.enter_password_again));
                } else if (!edtPassword.getText().toString().trim().equalsIgnoreCase(edtConfirmPassword.getText().toString().trim())) {
                    Utility.ShowToastMessage(context, getString(R.string.password_do_not_match));
                } else {
                    getItemListBySubCategoryAPI();
                }
                break;
            case R.id.llBtnLogin:
                finish();
                break;
        }
    }

    private void getItemListBySubCategoryAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud();
            SignUpRequest requestObject = new SignUpRequest();
            requestObject.setFirstName(edtFirstName.getText().toString().trim());
            requestObject.setLastName(edtLastName.getText().toString().trim());
            requestObject.setGender(spnGender.getSelectedItem().toString());
            requestObject.setEmail(edtEmail.getText().toString().trim());
            requestObject.setPhoneNumber(edtNumber.getText().toString().trim());
//            requestObject.setParentId();
            RetrofitClient.getAPIService().callRegisterUserAPI(requestObject).enqueue(new Callback<UserDetail>() {
                @Override
                public void onResponse(Call<UserDetail> call, Response<UserDetail> response) {
                    stopProgressHud();
                    if (response.code() == 200 && response.body() != null && response.body().getId() != null) {
                        SharedPref.setSharedPreference(context, Constants.USER_ID, response.body().getId());
                        SharedPref.setSharedPreference(context, Constants.USER_NAME, response.body().getFullName());
                        SharedPref.setSharedPreference(context, Constants.USER_EMAIL, response.body().getEmail());
                        SharedPref.setSharedPreference(context, Constants.USER_PHONE, response.body().getPhoneNumber());
                        finish();
                    } else {
                        Log.e(TAG, response.toString());
                        Utility.ShowToastMessage(context,response.message());
                    }
                }

                @Override
                public void onFailure(Call<UserDetail> call, Throwable t) {
                    stopProgressHud();
                    Log.e(TAG, t.getMessage(), t);
                }
            });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

}