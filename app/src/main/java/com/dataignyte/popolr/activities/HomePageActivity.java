package com.dataignyte.popolr.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.fragments.AddReferralFragment;
import com.dataignyte.popolr.fragments.DepartmentListFragment;
import com.dataignyte.popolr.fragments.MyOrdersFragment;
import com.dataignyte.popolr.fragments.MyTribeFragment;
import com.dataignyte.popolr.fragments.ProfileFragment;
import com.dataignyte.popolr.helper.SharedPref;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.APICaller.RetrofitClient;
import com.dataignyte.popolr.retrofit.model.response.CartResponse;
import com.dataignyte.popolr.retrofit.model.response.UserDetailsResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePageActivity extends BaseActivity implements View.OnClickListener {

    public static EditText edtSearch;
    public static ImageView ivBtnTxtToSpeech;
    private Context context;
    private DrawerLayout sideDrawer;
    private Fragment selectedFragment;
    private RelativeLayout rlSearchBox;
    private TextView txvToolbarTittle, txvCartCount;
    private boolean isUserLogedIn = false;
    private TextView txvName, txvEmail;
    private ImageView ivProfilePic;
    private String TAG = HomePageActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
        initialise();
    }

    private void initialise() {
        context = this;

        sideDrawer = (DrawerLayout) findViewById(R.id.sideDrawer);

        rlSearchBox = (RelativeLayout) findViewById(R.id.rlSearchBox);
        edtSearch = (EditText) findViewById(R.id.edtSearch);
        ivBtnTxtToSpeech = (ImageView) findViewById(R.id.ivBtnTxtToSpeech);
        txvCartCount = (TextView) findViewById(R.id.txvCartCount);
        txvToolbarTittle = (TextView) findViewById(R.id.txvToolbarTittle);

        txvName = findViewById(R.id.txvName);
        txvEmail = findViewById(R.id.txvEmail);
        ivProfilePic = findViewById(R.id.ivProfilePic);

        if (SharedPref.getSharedPreferences(context, Constants.TOKEN) != null
                && SharedPref.getSharedPreferences(context, Constants.TOKEN).length() != 0) {
            isUserLogedIn = true;
            ((TextView) findViewById(R.id.txvLoginLogout)).setText(R.string.logout);
            getUserDetails();

        } else {
            isUserLogedIn = false;
            ((TextView) findViewById(R.id.txvLoginLogout)).setText(R.string.login);
        }


        ((ImageView) findViewById(R.id.ivButtonDrawer)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.ivDrawerClose)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.ivButtonSearch)).setOnClickListener(this);
        ((RelativeLayout) findViewById(R.id.rlCart)).setOnClickListener(this);
        ((RelativeLayout) findViewById(R.id.rlDrawerBtnHome)).setOnClickListener(this);
        ((RelativeLayout) findViewById(R.id.rlDrawerBtnOrderHistory)).setOnClickListener(this);
        ((CardView) findViewById(R.id.cvProfile)).setOnClickListener(this);
        ((RelativeLayout) findViewById(R.id.rlDrawerBtnProfileDetails)).setOnClickListener(this);
        ((RelativeLayout) findViewById(R.id.rlDrawerBtnSettings)).setOnClickListener(this);
        ((RelativeLayout) findViewById(R.id.rlDrawerBtnHelp)).setOnClickListener(this);
        ((RelativeLayout) findViewById(R.id.rlDrawerBtnLogout)).setOnClickListener(this);
//        rlDrawerBtnAddReferral
        ((RelativeLayout) findViewById(R.id.rlDrawerBtnAddReferral)).setOnClickListener(this);

//        rlDrawerBtnMyTribes
        ((RelativeLayout) findViewById(R.id.rlDrawerBtnMyTribes)).setOnClickListener(this);


        setFragments(new DepartmentListFragment(), null, false);

        /*Redirecting order fragment if user coming after ordering*/
        String flag = getIntent().getStringExtra(Constants.FRAGMENT_FLAG);
        if (flag != null && !flag.isEmpty()) {
            if (flag.equalsIgnoreCase(Constants.FRAGMENT_ORDER_LIST)) {
                setToolbarTittle(R.string.order_history);
                setFragments(new MyOrdersFragment(), null, false);
            } else if (flag.equalsIgnoreCase(Constants.FRAGMENT_MY_TRIBES)) {
                setToolbarTittle(R.string.my_tribes);
                setFragments(new MyTribeFragment(), null, false);
            }
        }


    }

    public void setToolbarTittle(int tittle) {
        ((TextView) findViewById(R.id.txvToolbarTittle)).setText(context.getResources().getString(tittle));
    }

    public void setFragments(Fragment fragment, Bundle bundle, boolean addToBackStack) {
        if (fragment instanceof DepartmentListFragment) {
            setToolbarTittle(R.string.home);
            ((ImageView) findViewById(R.id.ivButtonSearch)).setVisibility(View.VISIBLE);
        } else
            ((ImageView) findViewById(R.id.ivButtonSearch)).setVisibility(View.GONE);
        selectedFragment = fragment;
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (bundle != null)
            fragment.setArguments(bundle);
        transaction.replace(R.id.framLayout, fragment, fragment.getClass().getName());
        if (addToBackStack)
            transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sideDrawer.closeDrawer(GravityCompat.START);
        rlSearchBox.setVisibility(View.GONE);
        txvToolbarTittle.setVisibility(View.VISIBLE);
    }

    private void showHideEditText() {
        if (rlSearchBox.isShown()) {
            rlSearchBox.setVisibility(View.GONE);
            txvToolbarTittle.setVisibility(View.VISIBLE);
        } else {
            rlSearchBox.setVisibility(View.VISIBLE);
            txvToolbarTittle.setVisibility(View.GONE);
        }
    }

    private void showLoginDialog() {
        new AlertDialog.Builder(context)
                .setMessage(R.string.do_you_want_to_login)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        sideDrawer.closeDrawer(GravityCompat.START);
                        startActivity(new Intent(context, LoginActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK & Intent.FLAG_ACTIVITY_NEW_TASK));
                        finishAffinity();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /* Toolbar buttons */
            case R.id.ivButtonDrawer:
                sideDrawer.openDrawer(GravityCompat.START);
                rlSearchBox.setVisibility(View.GONE);
                txvToolbarTittle.setVisibility(View.VISIBLE);
                break;
            case R.id.ivDrawerClose:
                sideDrawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.ivButtonSearch:
//                showHideEditText();
                startActivity(new Intent(context, ItemsListActivity.class).putExtra(Constants.DIRECT_FROM_HOME, true));
                break;
            case R.id.rlCart:
                startActivity(new Intent(context, MyCartActivity.class));
                break;

            /* Drawer Options */
            case R.id.rlDrawerBtnHome:
                sideDrawer.closeDrawer(GravityCompat.START);
                setFragments(new DepartmentListFragment(), null, false);
                break;
            case R.id.rlDrawerBtnOrderHistory:
                if (isUserLogedIn) {
                    sideDrawer.closeDrawer(GravityCompat.START);
                    setToolbarTittle(R.string.order_history);
                    setFragments(new MyOrdersFragment(), null, false);
                } else
                    showLoginDialog();
                break;
            case R.id.rlDrawerBtnMyTribes:
                if (isUserLogedIn) {
                    sideDrawer.closeDrawer(GravityCompat.START);
                    setToolbarTittle(R.string.my_tribes);
                    setFragments(new MyTribeFragment(), null, false);
                } else
                    showLoginDialog();
                break;

            case R.id.rlDrawerBtnAddReferral:
                if (isUserLogedIn) {
                    sideDrawer.closeDrawer(GravityCompat.START);
                    setToolbarTittle(R.string.add_referral);
                    setFragments(new AddReferralFragment(), null, false);
                } else
                    showLoginDialog();
                break;

            case R.id.rlDrawerBtnProfileDetails:
            case R.id.cvProfile:
                if (isUserLogedIn) {
                    setToolbarTittle(R.string.profile);
                    sideDrawer.closeDrawer(GravityCompat.START);
                    setFragments(new ProfileFragment(), null, false);
                } else
                    showLoginDialog();
                break;
            case R.id.rlDrawerBtnSettings:
                setToolbarTittle(R.string.settings);
                sideDrawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.rlDrawerBtnHelp:
                setToolbarTittle(R.string.help);
                sideDrawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.rlDrawerBtnLogout:
                if (isUserLogedIn) {
                    new AlertDialog.Builder(context)
                            .setMessage(R.string.you_want_to_exit)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    sideDrawer.closeDrawer(GravityCompat.START);
                                    SharedPref.clearPreference(context);
                                    startActivity(new Intent(context, LoginActivity.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK & Intent.FLAG_ACTIVITY_NEW_TASK));
                                    finish();
                                }
                            })
                            .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            })
                            .show();
                } else {
                    startActivity(new Intent(context, LoginActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK & Intent.FLAG_ACTIVITY_NEW_TASK));
                    finish();
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (selectedFragment != null)
            selectedFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onBackPressed() {
        if (selectedFragment instanceof DepartmentListFragment) {
            new android.app.AlertDialog.Builder(context)
                    .setMessage(R.string.do_you_want_to_exit)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    })
                    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    })
                    .show();
        } else
            setFragments(new DepartmentListFragment(), null, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        txvCartCount.setText(Utility.getCartItemsCount(context));
    }

    private void getUserDetails() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud();
            RetrofitClient.getAPIService().getUserDetails(SharedPref.getSharedPreferences(context, Constants.TOKEN))
                    .enqueue(new Callback<UserDetailsResponse>() {
                        @Override
                        public void onResponse(Call<UserDetailsResponse> call, Response<UserDetailsResponse> response) {
                            if (response.code() == 200 && response.body() != null && response.body().get_id() != null) {
                                txvName.setText(response.body().getFirstName() + " " + response.body().getLastName());
                                txvEmail.setText(response.body().getEmail());
                            } else {
                                Log.e(TAG, response.toString());
                                Utility.ShowToastMessage(context, response.message());
                            }
                            getMyCart();
                        }

                        @Override
                        public void onFailure(Call<UserDetailsResponse> call, Throwable t) {
                            stopProgressHud();
                            Log.e(TAG, t.getMessage(), t);
                        }
                    });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

    private void getMyCart() {
        if (Utility.isConnectingToInternet(context)) {
            RetrofitClient.getAPIService().getMyCart(SharedPref.getSharedPreferences(context, Constants.TOKEN)).enqueue(new Callback<CartResponse>() {
                @Override
                public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                    stopProgressHud();
                    if (response.code() == 200 && response.body() != null && response.body().getCart() != null) {
                        /*Saving the arrayList of items from server*/
                        SharedPref.saveItemsArrayListInSharedPref(context, response.body().getCart().getItems());
                        txvCartCount.setText(Utility.getCartItemsCount(context));
                    } else {
                        Log.e(TAG, response.toString());
                        Utility.ShowToastMessage(context, response.message());
                    }
                }

                @Override
                public void onFailure(Call<CartResponse> call, Throwable t) {
                    stopProgressHud();
                    Log.e(TAG, t.getMessage(), t);
                }
            });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }
}