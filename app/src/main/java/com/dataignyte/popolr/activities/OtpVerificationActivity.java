package com.dataignyte.popolr.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.helper.SharedPref;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.APICaller.RetrofitClient;
import com.dataignyte.popolr.retrofit.model.request.ChangePasswordRequest;
import com.dataignyte.popolr.retrofit.model.request.SignUpRequest;
import com.dataignyte.popolr.retrofit.model.response.SucessResult;
import com.dataignyte.popolr.retrofit.model.response.UserDetail;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpVerificationActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = OtpVerificationActivity.class.getSimpleName();
    private Context context;
    private EditText edtOtp1, edtOtp2, edtOtp3, edtOtp4, edtPassword, edtConfirmPassword;
    private LinearLayout llCHangePasswordForm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);
        initialise();
    }

    private void initialise () {
        context = this;
        edtOtp1 = (EditText) findViewById(R.id.edtOtp1);
        edtOtp2 = (EditText) findViewById(R.id.edtOtp2);
        edtOtp3 = (EditText) findViewById(R.id.edtOtp3);
        edtOtp4 = (EditText) findViewById(R.id.edtOtp4);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        edtConfirmPassword = (EditText) findViewById(R.id.edtConfirmPassword);
        ((TextView) findViewById(R.id.txvButtonSubmit)).setOnClickListener(this);

        setOtpTextChangeListener();
    }

    void setOtpTextChangeListener() {
        edtOtp1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length()==1)
                    Utility.setFocusAndOpenKeyBoard(context, edtOtp2);
            }
        });

        edtOtp2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length()==0)
                    Utility.setFocusAndOpenKeyBoard(context, edtOtp1);
                else if (s.length()==1)
                    Utility.setFocusAndOpenKeyBoard(context, edtOtp3);
            }
        });

        edtOtp3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length()==0)
                    Utility.setFocusAndOpenKeyBoard(context, edtOtp2);
                else if (s.length()==1)
                    Utility.setFocusAndOpenKeyBoard(context, edtOtp4);
            }
        });

        edtOtp4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length()==0)
                    Utility.setFocusAndOpenKeyBoard(context, edtOtp3);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txvButtonSubmit:
                if (edtOtp1.getText().toString().trim().isEmpty()
                        || edtOtp2.getText().toString().trim().isEmpty()
                        || edtOtp3.getText().toString().trim().isEmpty()
                        || edtOtp4.getText().toString().trim().isEmpty()) {
                    Utility.ShowToastMessage(context, getString(R.string.enter_otp));
                } else if (edtPassword.getText().toString().trim().isEmpty()) {
                    Utility.ShowToastMessage(context, getString(R.string.enter_password));
                } else if (edtPassword.getText().toString().trim().length()<5) {
                    Utility.ShowToastMessage(context, getString(R.string.enter_valid_password));
                } else if (edtConfirmPassword.getText().toString().trim().isEmpty()) {
                    Utility.ShowToastMessage(context, getString(R.string.enter_password_again));
                } else if (!edtPassword.getText().toString().trim().equalsIgnoreCase(edtConfirmPassword.getText().toString().trim())) {
                    Utility.ShowToastMessage(context, getString(R.string.password_do_not_match));
                } else {

                }
                break;
        }
    }

    private void getItemListBySubCategoryAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud();
            ChangePasswordRequest requestObject = new ChangePasswordRequest();
            requestObject.setOtp(edtOtp1.getText().toString().trim()+edtOtp2.getText().toString().trim()+edtOtp3.getText().toString().trim()+edtOtp4.getText().toString().trim() );
            requestObject.setPassowrd(edtConfirmPassword.getText().toString().trim());
            if (getIntent().getStringExtra(Constants.USER_PHONE)!=null)
                requestObject.setPhoneNumber(getIntent().getStringExtra(Constants.USER_PHONE));
            RetrofitClient.getAPIService().callChangePasswordAPI(requestObject).enqueue(new Callback<SucessResult>() {
                @Override
                public void onResponse(Call<SucessResult> call, Response<SucessResult> response) {
                    stopProgressHud();
                    if (response.code() == 200 && response.body() != null && response.body().getMessage() != null) {
                        Utility.ShowToastMessage(context, response.body().getMessage());
                        finish();
                    } else {
                        if (response.body()!=null && response.body().getError()!=null)
                            Utility.ShowToastMessage(context, response.body().getError());
                        Log.e(TAG, response.toString());
                    }
                }

                @Override
                public void onFailure(Call<SucessResult> call, Throwable t) {
                    stopProgressHud();
                    Log.e(TAG, t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }
}