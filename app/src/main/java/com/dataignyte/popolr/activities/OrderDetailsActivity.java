package com.dataignyte.popolr.activities;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.adapters.OrderDetailItemsAdapter;
import com.dataignyte.popolr.adapters.OrderSummaryAdapter;
import com.dataignyte.popolr.databinding.ActivityOrderDetailsBinding;
import com.dataignyte.popolr.helper.SharedPref;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.APICaller.RetrofitClient;
import com.dataignyte.popolr.retrofit.model.response.OrderDetailsModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsActivity extends BaseActivity {

    private final String TAG = OrderDetailsActivity.class.getSimpleName();
    ActivityOrderDetailsBinding binding;
    private Context context;
    private String orderId = "";
    private OrderSummaryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityOrderDetailsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        initialise();
    }

    private void initialise() {
        context = this;
        if (getIntent().getStringExtra(Constants.ORDER_ID) != null)
            orderId = getIntent().getStringExtra(Constants.ORDER_ID);

        callGetOrderDetailsAPI();

        binding.ivButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void callGetOrderDetailsAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud();
            RetrofitClient.getAPIService().getOrderDetailsAPI(SharedPref.getSharedPreferences(context, Constants.TOKEN), orderId)
                    .enqueue(new Callback<OrderDetailsModel>() {
                        @Override
                        public void onResponse(Call<OrderDetailsModel> call, Response<OrderDetailsModel> response) {
                            stopProgressHud();
                            if (response.code() == 200 && response.body() != null) {
                                setOrderDetails(response.body());
                            } else {
                                Log.e(TAG, response.message());
                                Utility.ShowToastMessage(context, getResources().getString(R.string.something_went_wrong));
                            }
                        }

                        @Override
                        public void onFailure(Call<OrderDetailsModel> call, Throwable t) {
                            stopProgressHud();
                            Log.e(TAG, t.getMessage(), t);
                            Utility.ShowToastMessage(context, getResources().getString(R.string.something_went_wrong));
                        }
                    });

        } else {
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
        }
    }

    private void setOrderDetails(OrderDetailsModel orderDetails) {
        binding.txvOrderId.setText(orderDetails.getId());
        binding.txvDeliveryAddress.setText(orderDetails.getAddress().getLine1() + "\n" +
                orderDetails.getAddress().getLine2() + "\n" +
                orderDetails.getAddress().getCity() + ",\t" + orderDetails.getAddress().getState()
                + "\nPin: " + orderDetails.getAddress().getPincode());
        binding.txvDeliverySlot.setText(orderDetails.getDeliverySlot().getStart() + " - " +
                orderDetails.getDeliverySlot().getEnd());
        binding.txvPaymentMethod.setText(orderDetails.getPaymentMode().getName());
        binding.txvSubTotal.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfDouble(orderDetails.getCart().getSubTotal()));
        binding.txvDiscount.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfDouble(orderDetails.getCart().getDiscountTotal()));
        binding.txvDeliverFee.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfDouble(orderDetails.getCart().getDeliveryCharge()));
        binding.txvRewardPoint.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfDouble(orderDetails.getCart().getRewardPointUsed()));
        binding.txvTotal.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfDouble(orderDetails.getCart().getTotal()));

        binding.rcvOrderItems.setLayoutManager(new LinearLayoutManager(context));
        binding.rcvOrderItems.setAdapter(new OrderDetailItemsAdapter(context, orderDetails.getCart().getItems()));
        setProgressBar(orderDetails.getStatus());

    }

    private void setProgressBar(String status) {
        if (status != null && !status.isEmpty()) {
            if (status.equalsIgnoreCase(Constants.TRANSIT)) {
                binding.viewTransitLeft.setBackgroundColor(R.color.colorPrimary);
                binding.cvTransit.setCardBackgroundColor(R.color.colorPrimary);
                binding.viewTransitRight.setBackgroundColor(R.color.colorPrimary);
            } else if (status.equalsIgnoreCase(Constants.DELIVERED)) {
                binding.viewTransitLeft.setBackgroundColor(R.color.colorPrimary);
                binding.cvTransit.setCardBackgroundColor(R.color.colorPrimary);
                binding.viewTransitRight.setBackgroundColor(R.color.colorPrimary);

                binding.viewDeliveredLeft.setBackgroundColor(R.color.colorPrimary);
                binding.cvDelivered.setCardBackgroundColor(R.color.colorPrimary);
                binding.viewDeliveredRight.setBackgroundColor(R.color.colorPrimary);
            }
        }

    }
}