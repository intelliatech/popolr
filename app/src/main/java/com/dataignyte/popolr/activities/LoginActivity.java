package com.dataignyte.popolr.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.helper.SharedPref;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.APICaller.RetrofitClient;
import com.dataignyte.popolr.retrofit.model.request.SignInRequest;
import com.dataignyte.popolr.retrofit.model.response.SignInResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private final String TAG = LoginActivity.class.getSimpleName();
    private Context context;
    private EditText edtMobileNumber, edtPassword;
    private TextView txvButtonLogin, txvButtonFrgtPswd;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initialise();
    }

    private void initialise() {
        context = this;
        edtMobileNumber = (EditText) findViewById(R.id.edtMobileNumber);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        txvButtonLogin = (TextView) findViewById(R.id.txvButtonLogin);
        txvButtonFrgtPswd = (TextView) findViewById(R.id.txvButtonFrgtPswd);

        txvButtonLogin.setOnClickListener(this);
        txvButtonFrgtPswd.setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.llBtnSignUp)).setOnClickListener(this);
    }

    private void clearData() {
        edtMobileNumber.setText("");
        edtPassword.setText("");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txvButtonLogin:
                if (edtMobileNumber.getText().toString().trim().isEmpty()) {
                    Utility.ShowToastMessage(context, getString(R.string.enter_your_number));
                    edtMobileNumber.setError(getString(R.string.enter_your_number));
                } else if (edtMobileNumber.getText().toString().trim().length()<10 ) {
                    Utility.ShowToastMessage(context, getString(R.string.enter_valid_number));
                    edtMobileNumber.setError(getString(R.string.enter_valid_number));
                } else if (edtPassword.getText().toString().trim().isEmpty()) {
                    Utility.ShowToastMessage(context, getString(R.string.enter_password));
                } else if (edtPassword.getText().toString().trim().length() < 5) {
                    Utility.ShowToastMessage(context, getString(R.string.enter_valid_password));
                } else {
                    callUserLoginAPI();
                }
                break;
            case R.id.txvButtonFrgtPswd:
                clearData();
                startActivity(new Intent(context, ForgetPasswordActivity.class));
                break;
            case R.id.llBtnSignUp:
                clearData();
                startActivity(new Intent(context, SignUpActivity.class));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(context)
                .setMessage(R.string.do_you_want_to_exit)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }

    private void callUserLoginAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud();
            SignInRequest signInRequest = new SignInRequest();
            signInRequest.setPhoneNumber(edtMobileNumber.getText().toString().trim());
            signInRequest.setPassword(edtPassword.getText().toString().trim());

            RetrofitClient.getAPIService().callSignInAPI(signInRequest).enqueue(new Callback<SignInResponse>() {
                @Override
                public void onResponse(Call<SignInResponse> call, Response<SignInResponse> response) {
                    stopProgressHud();
                    if (response.code() == 200 && response.body() != null && response.body().getToken() != null) {
                        SharedPref.setSharedPreference(context, Constants.TOKEN, response.body().getToken());

                        startActivity(new Intent(context, HomePageActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK & Intent.FLAG_ACTIVITY_NEW_TASK));
//                        SharedPref.setSharedPreference(context, Constants.ID, "1101");
                        finishAffinity();
                    } else {
                        Log.e(TAG, response.toString());
                        Utility.ShowToastMessage(context, response.message());
                    }
                }

                @Override
                public void onFailure(Call<SignInResponse> call, Throwable t) {
                    stopProgressHud();
                    Log.e(TAG, t.getMessage(), t);
                }
            });


        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }
}
