package com.dataignyte.popolr.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.adapters.ItemListAdapter;
import com.dataignyte.popolr.helper.OnListItemClicked;
import com.dataignyte.popolr.helper.SharedPref;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.APICaller.RetrofitClient;
import com.dataignyte.popolr.retrofit.model.response.ItemDetails;
import com.dataignyte.popolr.retrofit.model.response.ItemsList;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ItemsListActivity extends BaseActivity implements View.OnClickListener, OnListItemClicked {
    private static final String TAG = ItemsListActivity.class.getSimpleName();
    private Context context;
    private EditText edtSearch;
    private RecyclerView itemListRV;
    private ItemListAdapter itemListAdapter;
    private ArrayList<ItemDetails> itemsArrayList;
    private String subCategoryId = "";
    private TextView txvCartCount;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);
        initialise();
    }

    private void initialise() {
        context = this;

        edtSearch = (EditText) findViewById(R.id.edtSearch);
        itemListRV = (RecyclerView) findViewById(R.id.itemListRV);
        txvCartCount = (TextView) findViewById(R.id.txvCartCount);

        setEdtSearchTextChangeListener();
        itemListRV.setLayoutManager(new GridLayoutManager(context, 2));

        ((ImageView) findViewById(R.id.ivBtnTxtToSpeech)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.ivButtonBack)).setOnClickListener(this);
        ((RelativeLayout) findViewById(R.id.rlCart)).setOnClickListener(this);

        if (getIntent().getBooleanExtra(Constants.DIRECT_FROM_HOME, false)) {
            ((TextView) findViewById(R.id.txvSubCategoryName)).setText(getResources().getString(R.string.search_items));
            edtSearch.requestFocus();

        } else {
            ((TextView) findViewById(R.id.txvSubCategoryName)).setText(getIntent().getStringExtra(Constants.SUB_CATEGORY_NAME));
            subCategoryId = getIntent().getStringExtra(Constants.SUB_CATEGORY_ID);
            getItemListBySubCategoryAPI();
        }


    }

    private void setEdtSearchTextChangeListener() {
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() != 0) {
                    ((TextView) findViewById(R.id.txvSubCategoryName)).setText(getResources().getString(R.string.search_results));
                } else if (s.toString().trim().length() == 0) {
                    if (subCategoryId != null && !subCategoryId.isEmpty()) {
                        ((TextView) findViewById(R.id.txvSubCategoryName)).setText(getIntent().getStringExtra(Constants.SUB_CATEGORY_NAME));
                    } else {
                        itemListRV.setAdapter(null);
                        ((TextView) findViewById(R.id.txvSubCategoryName)).setText(getResources().getString(R.string.search_items));
                    }
                }
                if (itemListAdapter != null)
                    itemListAdapter.getFilter().filter(s.toString().trim());
            }
        });

        edtSearch.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN)) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_ENTER:
                        case EditorInfo.IME_ACTION_GO:
                            if (!edtSearch.getText().toString().isEmpty())
                                searchItemByNameAPI(edtSearch.getText().toString().trim());
                            return true;
                        default:
                            break;
                    }
                    return false;
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBtnTxtToSpeech:
                break;
            case R.id.ivButtonBack:
                finish();
                break;
            case R.id.rlCart:
                startActivity(new Intent(context, MyCartActivity.class));
                break;
        }
    }

    @Override
    public void onItemClicked(int position, View view, String value) {
        switch (view.getId()) {
            case R.id.cvBtnMinus:
                if (itemsArrayList.get(position).getQuantity() > 1) {
                    itemsArrayList.get(position).setQuantity((itemsArrayList.get(position).getQuantity() - 1));
                    itemListAdapter.notifyDataSetChanged();

                    /*Updating item in shared pref*/
                    SharedPref.saveItemInSharePref(context, itemsArrayList.get(position), Constants.DECREASE_ITEM);
                } else if (itemsArrayList.get(position).getQuantity() == 1) {
                    itemsArrayList.get(position).setQuantity(0);
                    itemListAdapter.notifyDataSetChanged();
                    /*Updating item in shared pref*/
                    SharedPref.saveItemInSharePref(context, itemsArrayList.get(position), Constants.DECREASE_ITEM);
                }
                updateCartItem();
                break;
            case R.id.cvBtnPlus:
                itemsArrayList.get(position).setQuantity((itemsArrayList.get(position).getQuantity() + 1));
                itemListAdapter.notifyDataSetChanged();
                /*Updating item in shared pref*/
                SharedPref.saveItemInSharePref(context, itemsArrayList.get(position), Constants.INCREASE_ITEM);
                updateCartItem();
                break;

            case R.id.txvBtnAddToCart:
                itemsArrayList.get(position).setQuantity((itemsArrayList.get(position).getQuantity() + 1));

                /*Saving added item into shared Pref*/
                SharedPref.saveItemInSharePref(context, itemsArrayList.get(position), Constants.ADD_ITEM);

                itemListAdapter.notifyDataSetChanged();
                /*String text = getResources().getString(R.string.do_you_want_to_add_this_item_to_cart, itemsArrayList.get(position).getQuantity());
                new AlertDialog.Builder(context)
                        .setMessage(text)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                SharedPref.saveItemInSharePref(context, itemsArrayList.get(position), Constants.ADD_ITEM);
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .show();*/
                updateCartItem();
                break;
        }

    }

    private void getItemListBySubCategoryAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud();
            RetrofitClient.getAPIService().getItemListBySubCategoryAPI(subCategoryId).enqueue(new Callback<ItemsList>() {
                @Override
                public void onResponse(Call<ItemsList> call, Response<ItemsList> response) {
                    stopProgressHud();
                    if (response.code() == 200 && response.body() != null && response.body().getItems() != null) {
                        itemsArrayList = response.body().getItems();
                        ArrayList<ItemDetails> itemsLocaList = SharedPref.getSharePrefItemsArraylist(context);
                        for (int i = 0; i < itemsArrayList.size(); i++) {
                            for (int j = 0; j < itemsLocaList.size(); j++) {
                                if (itemsArrayList.get(i).getId().equals(itemsLocaList.get(j).getId()))
                                    itemsArrayList.set(i, itemsLocaList.get(j));
                            }
                        }
                        itemListAdapter = new ItemListAdapter(context, itemsArrayList, ItemsListActivity.this);
                        itemListRV.setAdapter(itemListAdapter);
                    } else {
                        Log.e(TAG, response.toString());
                        Utility.ShowToastMessage(context, response.message());
                    }
                }

                @Override
                public void onFailure(Call<ItemsList> call, Throwable t) {
                    stopProgressHud();
                    Log.e(TAG, t.getMessage(), t);
                }
            });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

    private void searchItemByNameAPI(String searchKey) {
        if (Utility.isConnectingToInternet(context)) {
//            startProgressHud();
            RetrofitClient.getAPIService().searchItemByNameAPI(searchKey).enqueue(new Callback<ItemsList>() {
                @Override
                public void onResponse(Call<ItemsList> call, Response<ItemsList> response) {
                    stopProgressHud();
                    if (response.code() == 200 && response.body() != null && response.body().getItems() != null) {
                        itemsArrayList = response.body().getItems();
                        ArrayList<ItemDetails> itemsLocaList = SharedPref.getSharePrefItemsArraylist(context);
                        for (int i = 0; i < itemsArrayList.size(); i++) {
                            for (int j = 0; j < itemsLocaList.size(); j++) {
                                if (itemsArrayList.get(i).getId().equals(itemsLocaList.get(j).getId()))
                                    itemsArrayList.set(i, itemsLocaList.get(j));
                            }
                        }
                        itemListAdapter = new ItemListAdapter(context, itemsArrayList, ItemsListActivity.this);
                        itemListRV.setAdapter(itemListAdapter);
                    } else {
                        Log.e(TAG, response.toString());
                        Utility.ShowToastMessage(context, response.message());
                    }
                }

                @Override
                public void onFailure(Call<ItemsList> call, Throwable t) {
                    stopProgressHud();
                    Log.e(TAG, t.getMessage(), t);
                }
            });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

    @Override
    protected void onResume() {
        super.onResume();
        txvCartCount.setText(Utility.getCartItemsCount(context));

    }

    private void updateCartItem() {
        txvCartCount.setText(Utility.getCartItemsCount(context));
    }
}
