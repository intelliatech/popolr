package com.dataignyte.popolr.activities;

import android.animation.Animator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.adapters.CartItemsAdapter;
import com.dataignyte.popolr.helper.OnListItemClicked;
import com.dataignyte.popolr.helper.SharedPref;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.APICaller.RetrofitClient;
import com.dataignyte.popolr.retrofit.model.request.UpdateCartItemRequest;
import com.dataignyte.popolr.retrofit.model.response.Cart;
import com.dataignyte.popolr.retrofit.model.response.CartResponse;
import com.dataignyte.popolr.retrofit.model.response.ItemDetails;
import com.dataignyte.popolr.retrofit.model.response.SucessResult;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyCartActivity extends BaseActivity implements View.OnClickListener, OnListItemClicked {

    private final String TAG = MyCartActivity.class.getSimpleName();
    TextView headingTotal;
    private Context context;
    private TextView txvBtnCheckOut, txvNoDataFound, txvMyCartCount, txvPincode, txvTotalAmount, txvSubTotal, txvDelCharges, txvDiscountTotal, txvGrandTotal;
    private RecyclerView cartItemRV;
    private ArrayList<ItemDetails> itemsArrayListLocal = new ArrayList<>();
    private Cart cart;
    private CartItemsAdapter cartItemsAdapter;
    private boolean isUserLogedIn = false;
    private boolean flag = true;
    private CardView cvExtraInfo, cvCheckout;
    private boolean isCheckOutEnable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cart);
        initialise();
    }

    private void initialise() {
        context = this;

        if (SharedPref.getSharedPreferences(context, Constants.TOKEN) != null && SharedPref.getSharedPreferences(context, Constants.TOKEN).length() != 0) {
            isUserLogedIn = true;
        } else {
            isUserLogedIn = false;
        }

        txvNoDataFound = (TextView) findViewById(R.id.txvNoDataFound);
        txvMyCartCount = (TextView) findViewById(R.id.txvMyCartCount);
        txvPincode = (TextView) findViewById(R.id.txvPincode);
        txvTotalAmount = (TextView) findViewById(R.id.txvTotalAmount);
        cartItemRV = (RecyclerView) findViewById(R.id.cartItemRV);
        cartItemRV.setLayoutManager(new LinearLayoutManager(context));
        cvExtraInfo = (CardView) findViewById(R.id.cvExtraInfo);
        headingTotal = (TextView) findViewById(R.id.headingTotal);
        txvSubTotal = findViewById(R.id.txvSubTotal);
        txvDelCharges = findViewById(R.id.txvDelCharges);
        txvDiscountTotal = findViewById(R.id.txvDiscountTotal);
        txvGrandTotal = findViewById(R.id.txvGrandTotal);
        txvBtnCheckOut = findViewById(R.id.txvBtnCheckOut);
        cvCheckout = findViewById(R.id.cvCheckout);

        ((ImageView) findViewById(R.id.ivButtonBack)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.ivButtonHome)).setOnClickListener(this);
        ((RelativeLayout) findViewById(R.id.totalDetail)).setOnClickListener(this);
        ((TextView) findViewById(R.id.txvBtnCheckOut)).setOnClickListener(this);
        headingTotal.setOnClickListener(this);
        txvTotalAmount.setOnClickListener(this);
        /*Getting itemsArrayList from Shared pref */
        if (isUserLogedIn) {
            if (SharedPref.contains(context, Constants.ITEMS) && SharedPref.getSharePrefItemsArraylist(context) != null && SharedPref.getSharePrefItemsArraylist(context).size() != 0) {
                itemsArrayListLocal = SharedPref.getSharePrefItemsArraylist(context);
                setCartCount();
                updateLocalItemOnServer();
            } else
                getMyCart();
        } else if (SharedPref.contains(context, Constants.ITEMS) && SharedPref.getSharePrefItemsArraylist(context) != null) {
            itemsArrayListLocal = SharedPref.getSharePrefItemsArraylist(context);
            setCartCount();
            calculateTotal();
            cartItemsAdapter = new CartItemsAdapter(context, itemsArrayListLocal, this);
            cartItemRV.setAdapter(cartItemsAdapter);
        } else
            txvNoDataFound.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivButtonBack:
                finish();
                break;
            case R.id.ivButtonHome:
                startActivity(new Intent(context, HomePageActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP & Intent.FLAG_ACTIVITY_NEW_TASK));
                finishAffinity();
                break;
            case R.id.txvBtnCheckOut:
                if (isUserLogedIn) {
                    if (itemsArrayListLocal.size() != 0) {
                        if (isCheckOutEnable)
                            startActivity(new Intent(context, CheckOutActivity.class));
                        else
                            Utility.showSnackbar(context, getResources().getString(R.string.cart_total_is_below_minimum_cart_value));
                    } else
                        Utility.ShowToastMessage(context, getString(R.string.error_add_item_in_cart));
                } else
                    showLoginDialog();
                break;
            case R.id.totalDetail:
            case R.id.headingTotal:
            case R.id.txvTotalAmount:
                if (isUserLogedIn) {
                    // onSlideViewButtonClick();
                }
                break;
        }
    }

    @Override
    public void onItemClicked(int position, View view, String value) {
        switch (view.getId()) {
            case R.id.cvBtnMinus:
                if (itemsArrayListLocal.get(position).getQuantity() > 1) {
                    if (!isUserLogedIn) {
                        itemsArrayListLocal.get(position).setQuantity((itemsArrayListLocal.get(position).getQuantity() - 1));
                        cartItemsAdapter.notifyDataSetChanged();
                        calculateTotal();
                        SharedPref.saveItemInSharePref(context, itemsArrayListLocal.get(position), Constants.DECREASE_ITEM);
                    } else {
                        itemsArrayListLocal.get(position).setQuantity((itemsArrayListLocal.get(position).getQuantity() - 1));
                        cartItemsAdapter.notifyDataSetChanged();
                        calculateTotal();
                        updateLocalItemOnServer();
                    }
                } else if (itemsArrayListLocal.get(position).getQuantity() == 1) {
                    if (!isUserLogedIn) {
                        itemsArrayListLocal.remove(position);
                        cartItemsAdapter.notifyDataSetChanged();
                        calculateTotal();
                        SharedPref.saveItemsArrayListInSharedPref(context, itemsArrayListLocal);
                    } else {
                        new AlertDialog.Builder(context)
                                .setMessage("Do you want to remove this from cart?")
                                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        itemsArrayListLocal.get(position).setQuantity((itemsArrayListLocal.get(position).getQuantity() - 1));
                                        cartItemsAdapter.notifyDataSetChanged();
                                        calculateTotal();
                                        updateLocalItemOnServer();
                                    }
                                })
                                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                })
                                .show();
                    }
                }
                setCartCount();
                break;

            case R.id.cvBtnPlus:
                if (!isUserLogedIn) {
                    itemsArrayListLocal.get(position).setQuantity((itemsArrayListLocal.get(position).getQuantity() + 1));
                    cartItemsAdapter.notifyDataSetChanged();
                    calculateTotal();
                    SharedPref.saveItemInSharePref(context, itemsArrayListLocal.get(position), Constants.INCREASE_ITEM);
                } else {
                    itemsArrayListLocal.get(position).setQuantity((itemsArrayListLocal.get(position).getQuantity() + 1));
                    cartItemsAdapter.notifyDataSetChanged();
                    calculateTotal();
                    updateLocalItemOnServer();
                }
                setCartCount();
                break;
        }
    }

    private void getMyCart() {
        if (Utility.isConnectingToInternet(context)) {
            RetrofitClient.getAPIService().getMyCart(SharedPref.getSharedPreferences(context, Constants.TOKEN)).enqueue(new Callback<CartResponse>() {
                @Override
                public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {
                    stopProgressHud();
                    if (response.code() == 200 && response.body() != null && response.body().getCart() != null) {
                        cart = response.body().getCart();
                        itemsArrayListLocal = response.body().getCart().getItems();
                        cartItemsAdapter = new CartItemsAdapter(context, itemsArrayListLocal, MyCartActivity.this);
                        cartItemRV.setAdapter(cartItemsAdapter);
                        calculateTotal();

                        /*Saving the arrayList of items from server*/
                        SharedPref.saveItemsArrayListInSharedPref(context, itemsArrayListLocal);
                        setCartCount();

                        if (itemsArrayListLocal.size() == 0)
                            txvNoDataFound.setVisibility(View.VISIBLE);
                        else
                            txvNoDataFound.setVisibility(View.GONE);

                    } else {
                        Log.e(TAG, response.toString());
                        Utility.ShowToastMessage(context, response.message());
                    }
                }

                @Override
                public void onFailure(Call<CartResponse> call, Throwable t) {
                    stopProgressHud();
                    Log.e(TAG, t.getMessage(), t);
                }
            });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

    private void updateLocalItemOnServer() {
        if (Utility.isConnectingToInternet(context)) {
            UpdateCartItemRequest updateCartItemRequest = new UpdateCartItemRequest();
            ArrayList<UpdateCartItemRequest.Item> arrayListItems = new ArrayList<>();
            for (int i = 0; i < itemsArrayListLocal.size(); i++) {
                UpdateCartItemRequest.Item item = new UpdateCartItemRequest.Item();
                item.setId(itemsArrayListLocal.get(i).getId());
                item.setQuantity(itemsArrayListLocal.get(i).getQuantity());
                arrayListItems.add(item);
            }
            updateCartItemRequest.setItems(arrayListItems);

            RetrofitClient.getAPIService().addUpdateCart(SharedPref.getSharedPreferences(context, Constants.TOKEN), updateCartItemRequest).enqueue(new Callback<SucessResult>() {
                @Override
                public void onResponse(Call<SucessResult> call, Response<SucessResult> response) {
                    stopProgressHud();
                    if (response.code() == 200 && response.body() != null && response.body().getMessage().equalsIgnoreCase("success")) {
                        Log.i(TAG, response.body().getMessage());
                        getMyCart();
                    } else {
                        Log.i(TAG, response.message());
                    }
                }

                @Override
                public void onFailure(Call<SucessResult> call, Throwable t) {
                    stopProgressHud();
                    Log.i(TAG, t.getMessage(), t);
                }
            });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

    private void showLoginDialog() {
        new AlertDialog.Builder(context)
                .setMessage(R.string.do_you_want_to_login)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startActivity(new Intent(context, LoginActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK & Intent.FLAG_ACTIVITY_NEW_TASK));
                        finishAffinity();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }

    public void calculateTotal() {

        if (!isUserLogedIn) {
            double totalPrice = 0.00;
            for (int i = 0; i < itemsArrayListLocal.size(); i++) {
                totalPrice = totalPrice + (itemsArrayListLocal.get(i).getSellPrice() * itemsArrayListLocal.get(i).getQuantity());
            }
            txvTotalAmount.setText(getResources().getString(R.string.no_available));
            txvSubTotal.setText(getResources().getString(R.string.no_available));
            txvDelCharges.setText(getResources().getString(R.string.no_available));
            txvDiscountTotal.setText(getResources().getString(R.string.no_available));
            txvGrandTotal.setText(getResources().getString(R.string.no_available));
            txvTotalAmount.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfDouble(totalPrice));

            if (totalPrice < 200) {
                cvCheckout.setCardBackgroundColor(getResources().getColor(R.color.colorGrey));
                isCheckOutEnable = false;
            } else {
                cvCheckout.setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));
                isCheckOutEnable = true;
            }
        } else {
            txvSubTotal.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfDouble(cart.getSubTotal()));
            txvDelCharges.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfDouble(cart.getDeliveryCharge()));
            txvDiscountTotal.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfDouble(cart.getDiscountTotal()));
            txvGrandTotal.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfDouble(cart.getTotal()));
            txvTotalAmount.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfDouble(cart.getTotal()));
            if (cart.getTotal() < 200) {
                cvCheckout.setCardBackgroundColor(getResources().getColor(R.color.colorGrey));
                isCheckOutEnable = false;
            } else {
                cvCheckout.setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));
                isCheckOutEnable = true;
            }
        }
    }

    private void setCartCount() {
        txvMyCartCount.setText("My Cart (" + Utility.getCartItemsCount(context) + ")");
    }

    private void onSlideViewButtonClick() {
        if (!flag) {
            cvExtraInfo.animate().translationY(cvExtraInfo.getHeight()).setDuration(300).alpha(0.0f).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    cvExtraInfo.setVisibility(View.GONE);
                    flag = true;
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        } else {
            cvExtraInfo.animate().translationY(0).setDuration(300).alpha(1.0f).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    cvExtraInfo.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    flag = false;
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }
    }
}