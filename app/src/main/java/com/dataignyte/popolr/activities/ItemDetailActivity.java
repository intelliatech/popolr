package com.dataignyte.popolr.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.helper.SharedPref;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.APICaller.RetrofitClient;
import com.dataignyte.popolr.retrofit.model.response.ItemDetails;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.dataignyte.popolr.R.drawable;
import static com.dataignyte.popolr.R.id;
import static com.dataignyte.popolr.R.layout;

public class ItemDetailActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = ItemDetailActivity.class.getSimpleName();
    private Context context;
    private TextView txvToolbarItemName, txvItemName, txvItemCount, txvSellingPrice, txvMrp, txvDiscountPercent, txvItemDescription;
    private ImageView ivItemImage, ivButtonLike;
    private String itemId = "";
    private ItemDetails itemDetailObject;
    private TextView txvBtnAddToCart, txvCartCount;
    private LinearLayout llChangeCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_item_detail);
        initialise();
    }

    private void initialise() {
        context = this;
        itemId = getIntent().getStringExtra(Constants.ITEM_ID);

        txvToolbarItemName = (TextView) findViewById(id.txvToolbarTittle);
        txvItemName = (TextView) findViewById(id.txvItemName);
        txvCartCount = (TextView) findViewById(R.id.txvCartCount);

        txvSellingPrice = (TextView) findViewById(id.txvSellingPrice);
        txvMrp = (TextView) findViewById(id.txvMrp);
        txvDiscountPercent = (TextView) findViewById(id.txvDiscountPercent);

        txvItemCount = (TextView) findViewById(id.txvItemCount);
        txvItemDescription = (TextView) findViewById(id.txvItemDescription);
        ivItemImage = (ImageView) findViewById(id.ivItemImage);
        ivButtonLike = (ImageView) findViewById(id.ivButtonLike);
        txvBtnAddToCart = (TextView) findViewById(id.txvBtnAddToCart);
        llChangeCount = (LinearLayout) findViewById(R.id.llChangeCount);

        ivButtonLike.setOnClickListener(this);
        ((ImageView) findViewById(id.ivButtonBack)).setOnClickListener(this);
        ((RelativeLayout) findViewById(id.rlCart)).setOnClickListener(this);
        ((CardView) findViewById(id.cvBtnMinus)).setOnClickListener(this);
        ((CardView) findViewById(id.cvBtnPlus)).setOnClickListener(this);
        ((TextView) findViewById(id.txvBtnAddToCart)).setOnClickListener(this);
        getItemDetailsAPI();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case id.ivButtonBack:
                finish();
                break;
            case id.ivButtonLike:
                ivButtonLike.setImageDrawable(context.getResources().getDrawable(drawable.ic_like));
                break;
            case id.rlCart:
                startActivity(new Intent(context, MyCartActivity.class));
                break;
            case id.cvBtnMinus:
                if (itemDetailObject.getQuantity() > 1) {
                    itemDetailObject.setQuantity((itemDetailObject.getQuantity() - 1));
                    txvItemCount.setText(String.valueOf(itemDetailObject.getQuantity()));
                    customiseButtons(itemDetailObject.getQuantity());
                    SharedPref.saveItemInSharePref(context, itemDetailObject, Constants.DECREASE_ITEM);

                } else if (itemDetailObject.getQuantity() == 1) {
                    itemDetailObject.setQuantity(0);
                    customiseButtons(itemDetailObject.getQuantity());
                    /*Updating item in shared pref*/
                    SharedPref.saveItemInSharePref(context, itemDetailObject, Constants.DECREASE_ITEM);
                }
                updateCartItem();
                break;
            case id.cvBtnPlus:
                itemDetailObject.setQuantity((itemDetailObject.getQuantity() + 1));
                txvItemCount.setText(String.valueOf(itemDetailObject.getQuantity()));
                /*Updating item in shared pref*/
                SharedPref.saveItemInSharePref(context, itemDetailObject, Constants.INCREASE_ITEM);
                updateCartItem();
                break;
            case id.txvBtnAddToCart:
                itemDetailObject.setQuantity((itemDetailObject.getQuantity() + 1));
                customiseButtons(itemDetailObject.getQuantity());

                /*Updating item in shared pref*/
                SharedPref.saveItemInSharePref(context, itemDetailObject, Constants.ADD_ITEM);
                /* *//*Saving added item into shared Pref*//*
                Resources res = getResources();
                String text = res.getString(R.string.do_you_want_to_add_this_item_to_cart, itemDetailObject.getQuantity());
                new AlertDialog.Builder(context)
                        .setMessage(text)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                SharedPref.saveItemInSharePref(context, itemDetailObject, Constants.ADD_ITEM);
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .show();*/
                updateCartItem();

                break;
        }
    }

    private void getItemDetailsAPI() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud();
            RetrofitClient.getAPIService().getItemDetailsAPI(itemId).enqueue(new Callback<ItemDetails>() {
                @Override
                public void onResponse(Call<ItemDetails> call, Response<ItemDetails> response) {
                    stopProgressHud();
                    if (response.code() == 200 && response.body() != null && response.body().getId() != null) {


                        ArrayList<ItemDetails> itemsLocaList = SharedPref.getSharePrefItemsArraylist(context);
                        boolean found = false;
                        for (int i = 0; i < itemsLocaList.size(); i++) {
                            if (response.body().getId().equals(itemsLocaList.get(i).getId())) {
                                itemDetailObject = itemsLocaList.get(i);
                                found = true;
                                Log.i(TAG, "found=true");
                                break;
                            }
                            Log.i(TAG, "found=false");

                        }
                        if (!found) {
                            itemDetailObject = response.body();
                            Log.i(TAG, "found=1");
                        }
                        customiseButtons(itemDetailObject.getQuantity());
                        txvToolbarItemName.setText(itemDetailObject.getName());
                        txvItemName.setText(itemDetailObject.getName());
                        txvItemDescription.setText(itemDetailObject.getDescription());

                        if (itemDetailObject.getImages() != null && itemDetailObject.getImages().size() != 0)
                            Glide.with(context).load(Constants.URL.BASE_URL + itemDetailObject.getImages().get(0).replaceAll(" ", "%20")).into(ivItemImage);

                        txvSellingPrice.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfDouble(itemDetailObject.getSellPrice()));
                        txvMrp.setText(Constants.RUPEE + " " + Utility.get2DecimalStringOfDouble(itemDetailObject.getMrp()));
                        txvMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                        txvDiscountPercent.setText(Utility.get2DecimalStringOfDouble(itemDetailObject.getDiscountPercent()) + "% OFF");
                    } else {
                        Log.e(TAG, response.toString());
                        Utility.ShowToastMessage(context, response.message());
                    }
                }

                @Override
                public void onFailure(Call<ItemDetails> call, Throwable t) {
                    stopProgressHud();
                    Log.e(TAG, t.getMessage());
                }
            });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

    private void customiseButtons(int itemCount) {
        if (itemCount == 0) {
            txvBtnAddToCart.setVisibility(View.VISIBLE);
            llChangeCount.setVisibility(View.GONE);
        } else {
            txvBtnAddToCart.setVisibility(View.GONE);
            llChangeCount.setVisibility(View.VISIBLE);
        }
        txvItemCount.setText(String.valueOf(itemDetailObject.getQuantity()));
    }


    private void updateCartItem() {
        txvCartCount.setText(Utility.getCartItemsCount(context));
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateCartItem();
    }
}