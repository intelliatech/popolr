package com.dataignyte.popolr.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.APICaller.RetrofitClient;
import com.dataignyte.popolr.retrofit.model.response.SucessResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPasswordActivity extends BaseActivity implements View.OnClickListener {

    private String TAG  = ForgetPasswordActivity.class.getSimpleName();
    private Context context;
    private EditText edtNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        initialise();
    }

    private void initialise() {
        context = this;
        edtNumber = (EditText) findViewById(R.id.edtNumber);
        ((TextView) findViewById(R.id.txvButtonSubmit)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txvButtonSubmit:
                if (edtNumber.getText().toString().trim().isEmpty()) {
                    Utility.ShowToastMessage(context, getString(R.string.enter_your_number));
                    edtNumber.setError(getString(R.string.enter_your_number));
                } else if (edtNumber.getText().toString().trim().length() < 10) {
                    Utility.ShowToastMessage(context, getString(R.string.enter_valid_number));
                    edtNumber.setError(getString(R.string.enter_valid_number));
                } else {
                    sendOTPtoChangePasswordAPI(edtNumber.getText().toString().trim());
                }
                break;
        }
    }

    private void sendOTPtoChangePasswordAPI(String mobileNo) {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud();
            RetrofitClient.getAPIService().sendOTPtoChangePasswordAPI(mobileNo)
                    .enqueue(new Callback<SucessResult>() {
                        @Override
                        public void onResponse(Call<SucessResult> call, Response<SucessResult> response) {
                            stopProgressHud();
                            if (response.code() == 200 && response.body() != null && response.body().getMessage() != null) {
                                Utility.ShowToastMessage(context, response.body().getMessage());
                                startActivity(new Intent(context, OtpVerificationActivity.class)
                                        .putExtra(Constants.USER_PHONE, mobileNo));
                                finish();
                            } else {
                                if (response.message()!=null)
                                    Utility.ShowToastMessage(context, response.message());
                                    Log.e(TAG, response.toString());
                            }
                        }

                        @Override
                        public void onFailure(Call<SucessResult> call, Throwable t) {
                            stopProgressHud();
                            Log.e(TAG, t.getMessage());
                        }
                    });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }
}