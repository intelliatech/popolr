package com.dataignyte.popolr.activities;

import android.animation.Animator;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.dataignyte.popolr.R;
import com.dataignyte.popolr.fragments.DeliveryDetailsFragment;
import com.dataignyte.popolr.fragments.PaymentDetailsFragment;
import com.dataignyte.popolr.helper.Utility;

public class CheckOutActivity extends BaseActivity implements View.OnClickListener {

    public static TextView txvTotalAmount, txvSubTotal, txvDelCharges, txvDiscountTotal, txvGrandTotal;
    private Context context;
    private TextView txvTabDelivery, txvTabPayment;
    private Fragment selectedFragment;
    private CardView cvExtraInfo;
    private boolean isDetailsVisible = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        initialise();
    }

    private void initialise() {
        context = this;
        txvTabDelivery = (TextView) findViewById(R.id.txvTabDelivery);
        txvTabPayment = (TextView) findViewById(R.id.txvTabPayment);

        txvTotalAmount = (TextView) findViewById(R.id.txvTotalAmount);
        txvSubTotal = (TextView) findViewById(R.id.txvSubTotal);
        txvDelCharges = (TextView) findViewById(R.id.txvDelCharges);
        txvDiscountTotal = (TextView) findViewById(R.id.txvDiscountTotal);
        txvGrandTotal = (TextView) findViewById(R.id.txvGrandTotal);
        cvExtraInfo = (CardView) findViewById(R.id.cvExtraInfo);

        txvTabDelivery.setOnClickListener(this);
        txvTabPayment.setOnClickListener(this);
        ((RelativeLayout) findViewById(R.id.rlTotalInfo)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.ivButtonBack)).setOnClickListener(this);
        ((TextView) findViewById(R.id.txvBtnConfirmOrder)).setOnClickListener(this);
        setFragments(new DeliveryDetailsFragment(), null, false);
        ((TextView) findViewById(R.id.headingTotal)).setOnClickListener(this);
    }

    private void showSelectedTab(int selected) {
        if (selected == 1) {
            txvTabDelivery.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.ic_next);
            txvTabDelivery.setTextColor(Color.BLACK);
            txvTabDelivery.setTextSize(16.0f);

            txvTabPayment.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            txvTabPayment.setTextColor(Color.GRAY);
            txvTabPayment.setTextSize(15.0f);
            ((TextView) findViewById(R.id.txvBtnConfirmOrder)).setText(context.getString(R.string.next));

        } else {
            txvTabPayment.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.ic_previous);
            txvTabPayment.setTextColor(Color.BLACK);
            txvTabPayment.setTextSize(16.0f);

            txvTabDelivery.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            txvTabDelivery.setTextColor(Color.GRAY);
            txvTabDelivery.setTextSize(15.0f);
            ((TextView) findViewById(R.id.txvBtnConfirmOrder)).setText(context.getString(R.string.confirm_order));

        }
    }

    public void setFragments(Fragment fragment, Bundle bundle, boolean addToBackStack) {
        selectedFragment = fragment;
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (bundle != null)
            fragment.setArguments(bundle);
        transaction.replace(R.id.framLayout, fragment, fragment.getClass().getName());
        if (addToBackStack)
            transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivButtonBack:
                finish();
                break;
            case R.id.txvTabDelivery:
                showSelectedTab(1);
                setFragments(new DeliveryDetailsFragment(), null, false);
                break;
            case R.id.txvTabPayment:
                showSelectedTab(2);
                setFragments(new PaymentDetailsFragment(), null, false);
                break;
            case R.id.txvBtnConfirmOrder:
                if (DeliveryDetailsFragment.orderItemsPresent) {
                    if (selectedFragment instanceof PaymentDetailsFragment)
                        ((PaymentDetailsFragment) selectedFragment).checkPaymentConstrainst();
                    else {
                        showSelectedTab(2);
                        setFragments(new PaymentDetailsFragment(), null, false);
//                        Utility.ShowToastMessage(context, getString(R.string.complete_to_order_details));
                        Utility.showSnackbar(context, getResources().getString(R.string.complete_to_order_details));
                    }
                }
                break;

            case R.id.rlTotalInfo:
            case R.id.headingTotal:
                onSlideViewButtonClick();
                break;
        }
    }

    private void onSlideViewButtonClick() {
        if (!isDetailsVisible) {
            cvExtraInfo.animate().translationY(cvExtraInfo.getHeight()).setDuration(300).alpha(0.0f).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    cvExtraInfo.setVisibility(View.GONE);
                    isDetailsVisible = true;
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        } else {
            cvExtraInfo.animate().translationY(0).setDuration(300).alpha(1.0f).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    cvExtraInfo.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    isDetailsVisible = false;
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }
    }
}