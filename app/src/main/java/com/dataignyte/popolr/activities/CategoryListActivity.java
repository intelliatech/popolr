package com.dataignyte.popolr.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.R;
import com.dataignyte.popolr.adapters.CategoryListAdapter;
import com.dataignyte.popolr.adapters.SubCategoryListAdapter;
import com.dataignyte.popolr.helper.OnListItemClicked;
import com.dataignyte.popolr.helper.Utility;
import com.dataignyte.popolr.retrofit.APICaller.RetrofitClient;
import com.dataignyte.popolr.retrofit.model.response.*;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryListActivity extends BaseActivity implements View.OnClickListener, OnListItemClicked {

    private final String TAG = CategoryListActivity.class.getSimpleName();
    private RecyclerView categoryListRv, subCategoryListRv;
    private Context context;
    private ArrayList<Category> arrayListCategory = new ArrayList<>();
    private ArrayList<SubCategory> arrayListSubCategory = new ArrayList<>();
    private CategoryListAdapter categoryListAdapter;
    private SubCategoryListAdapter subCategoryListAdapter;
    private String departmentId = "";
    private TextView txvCartCount;
    private RelativeLayout rlCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);
        initialise();
    }

    private void initialise() {
        context = this;
        departmentId = getIntent().getStringExtra(Constants.DEPARTMENT_ID);
        categoryListRv = (RecyclerView) findViewById(R.id.categoryListRv);
        subCategoryListRv = (RecyclerView) findViewById(R.id.subCategoryListRv);
        ((TextView) findViewById(R.id.txvToolbarTittle)).setText(getIntent().getStringExtra(Constants.DEPARTMENT_NAME));

        ((ImageView) findViewById(R.id.ivButtonBack)).setOnClickListener(this);
        ((RelativeLayout) findViewById(R.id.rlCart)).setOnClickListener(this);
        txvCartCount=findViewById(R.id.txvCartCount);

        getCategoryList();

        categoryListRv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        subCategoryListRv.setLayoutManager(new GridLayoutManager(context, 2));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivButtonBack:
                finish();
                break;
            case R.id.rlCart:
                startActivity(new Intent(context, MyCartActivity.class));
                break;
        }
    }

    private void getCategoryList() {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud();
            RetrofitClient.getAPIService().callCategoryListByDepartment(departmentId).enqueue(new Callback<CategoryList>() {
                @Override
                public void onResponse(Call<CategoryList> call, Response<CategoryList> response) {
                    stopProgressHud();
                    if (response.code() == 200 && response.body() != null && response.body().getCategories() != null) {
                        arrayListCategory = response.body().getCategories();
                        categoryListAdapter = new CategoryListAdapter(context, arrayListCategory, CategoryListActivity.this);
                        categoryListRv.setAdapter(categoryListAdapter);

                        getSubCategoryList(arrayListCategory.get(0).getId());
                    } else {
                        Log.e(TAG, response.toString());
                        Utility.ShowToastMessage(context,response.message());
                    }
                }

                @Override
                public void onFailure(Call<CategoryList> call, Throwable t) {
                    stopProgressHud();
                    Log.e(TAG, t.getMessage(), t);
                }
            });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

    private void getSubCategoryList(String categoryId) {
        if (Utility.isConnectingToInternet(context)) {
            startProgressHud();
            RetrofitClient.getAPIService().getSubCategoryListByCategoryAPI(categoryId)
                    .enqueue(new Callback<SubCategoryList>() {
                @Override
                public void onResponse(Call<SubCategoryList> call, Response<SubCategoryList> response) {
                    stopProgressHud();
                    if (response.code() == 200 && response.body() != null && response.body().getSubCategories() != null) {
                        arrayListSubCategory = response.body().getSubCategories();
                        subCategoryListAdapter = new SubCategoryListAdapter(context, arrayListSubCategory, CategoryListActivity.this);
                        subCategoryListRv.setAdapter(subCategoryListAdapter);
                    }
                    else {
                        Log.e(TAG, response.toString());
                        Utility.ShowToastMessage(context,response.message());
                    }
                }

                @Override
                public void onFailure(Call<SubCategoryList> call, Throwable t) {
                    stopProgressHud();
                    Log.e(TAG, t.getMessage(), t);
                }
            });
        } else
            Utility.ShowToastMessage(context, getResources().getString(R.string.no_internet_connection));
    }

    @Override
    public void onItemClicked(int position, View view, String value) {
        switch (view.getId()) {
            case R.id.rlBtnCategory:
                categoryListAdapter.selectedPosition = position;
                categoryListAdapter.notifyDataSetChanged();
                getSubCategoryList(arrayListCategory.get(position).getId());
                break;
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        txvCartCount.setText(Utility.getCartItemsCount(context));
    }
}