package com.dataignyte.popolr.retrofit.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OrderListModel {
    @SerializedName("_id")
    @Expose
    String _id;
    @SerializedName("numberOfItems")
    @Expose
    String numberOfItems;
    @SerializedName("orderPlacedDate")
    @Expose
    String orderPlacedDate;
    @SerializedName("status")
    @Expose
    String status;
    @SerializedName("total")
    @Expose
    Double total;

    public String get_id() {
        return _id;
    }

    public String getNumberOfItems() {
        return numberOfItems;
    }

    public String getOrderPlacedDate() {
        return orderPlacedDate;
    }

    public String getStatus() {
        return status;
    }

    public Double getTotal() {
        return total;
    }
}
