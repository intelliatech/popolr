package com.dataignyte.popolr.retrofit.model.response;

import com.dataignyte.popolr.helper.Utility;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ChildModal {

    @SerializedName("_id")
    @Expose
    String _id;
    
    @SerializedName("fullName")
    @Expose
    String fullName;
    
    @SerializedName("firstName")
    @Expose
    String firstName;
    
    @SerializedName("lastName")
    @Expose
    String lastName;
    
    @SerializedName("gender")
    @Expose
    String gender;
    
    @SerializedName("phoneNumber")
    @Expose
    long phoneNumber;
    
    @SerializedName("email")
    @Expose
    String email;
    
    @SerializedName("parentId")
    @Expose
    String parentId;
    
    @SerializedName("orderTotal")
    @Expose
    double orderTotal;
    
    @SerializedName("rewardPoints")
    @Expose
    double rewardPoints;
    
    @SerializedName("childs")
    @Expose
    ArrayList<ChildModal> childs;

    public String get_id() {
        return Utility.geNonNullString(_id);
    }

    public String getFullName() {
        return Utility.geNonNullString(fullName);
    }

    public String getFirstName() {
        return Utility.geNonNullString(firstName);
    }

    public String getLastName() {
        return Utility.geNonNullString(lastName);
    }

    public String getGender() {
        return Utility.geNonNullString(gender);
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return Utility.geNonNullString(email);
    }

    public String getParentId() {
        return parentId;
    }

    public double getOrderTotal() {
        return orderTotal;
    }

    public double getRewardPoints() {
        return rewardPoints;
    }

    public ArrayList<ChildModal> getChilds() {
        return childs;
    }
}
