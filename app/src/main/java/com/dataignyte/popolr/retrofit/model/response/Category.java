package com.dataignyte.popolr.retrofit.model.response;

import com.dataignyte.popolr.helper.Utility;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Category {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("images")
    @Expose
    private ArrayList<String> images = null;
    @SerializedName("department")
    @Expose
    private Department department;
    @SerializedName("subCategoryCount")
    @Expose
    private int subCategoryCount;

    public String getId() {
        return Utility.geNonNullString(id);
    }

    public String getName() {
        return Utility.geNonNullString(name);
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public Department getDepartment() {
        return department;
    }

    public int getSubCategoryCount() {
        return subCategoryCount;
    }
}
