package com.dataignyte.popolr.retrofit.model.response;

import com.dataignyte.popolr.helper.Utility;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OrderCart {
    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("items")
    @Expose
    private ArrayList<OrderItem> items = null;

    @SerializedName("subTotal")
    @Expose
    private double subTotal;

    @SerializedName("total")
    @Expose
    private double total;

    @SerializedName("deliveryCharge")
    @Expose
    private int deliveryCharge;

    @SerializedName("discountTotal")
    @Expose
    private double discountTotal;

    @SerializedName("rewardPointUsed")
    @Expose
    private int rewardPointUsed;

    @SerializedName("totalAfterRewardPoint")
    @Expose
    private double totalAfterRewardPoint;

    public String getId() {
        return Utility.geNonNullString(id);
    }

    public ArrayList<OrderItem> getItems() {
        return items;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public double getTotal() {
        return total;
    }

    public int getDeliveryCharge() {
        return deliveryCharge;
    }

    public double getDiscountTotal() {
        return discountTotal;
    }

    public int getRewardPointUsed() {
        return rewardPointUsed;
    }

    public double getTotalAfterRewardPoint() {
        return totalAfterRewardPoint;
    }
}
