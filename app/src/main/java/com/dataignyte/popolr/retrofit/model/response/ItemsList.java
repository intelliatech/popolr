package com.dataignyte.popolr.retrofit.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ItemsList {
    @SerializedName("items")
    @Expose
    private ArrayList<ItemDetails> items = new ArrayList<>();

    public ArrayList<ItemDetails> getItems() {
        return items;
    }
}
