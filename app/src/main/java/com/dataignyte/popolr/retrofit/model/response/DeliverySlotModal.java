package com.dataignyte.popolr.retrofit.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeliverySlotModal {

    @SerializedName("_id")
    @Expose
    String _id;
    @SerializedName("name")
    @Expose
    String name;
    @SerializedName("start")
    @Expose
    String start;
    @SerializedName("end")
    @Expose
    String end;

    @SerializedName("isSelected")
    @Expose
    boolean isSelected = false;

    public String get_id() {
        return _id;
    }

    public String getName() {
        return name;
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
