package com.dataignyte.popolr.retrofit.model.response;

import com.dataignyte.popolr.retrofit.model.PaymentModeModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDetailsModel {

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("cart")
    @Expose
    private OrderCart cart;

    @SerializedName("placedDate")
    @Expose
    private String placedDate;

    @SerializedName("user")
    @Expose
    private UserDetail user;

    @SerializedName("address")
    @Expose
    private AddressModal address;

    @SerializedName("deliverySlot")
    @Expose
    private DeliverySlotModal deliverySlot;

    @SerializedName("paymentMode")
    @Expose
    private PaymentModeModel paymentMode;

    @SerializedName("status")
    @Expose
    private String status;

    public String getId() {
        return id;
    }

    public OrderCart getCart() {
        return cart;
    }

    public String getPlacedDate() {
        return placedDate;
    }

    public UserDetail getUser() {
        return user;
    }

    public AddressModal getAddress() {
        return address;
    }

    public DeliverySlotModal getDeliverySlot() {
        return deliverySlot;
    }

    public PaymentModeModel getPaymentMode() {
        return paymentMode;
    }

    public String getStatus() {
        return status;
    }
}
