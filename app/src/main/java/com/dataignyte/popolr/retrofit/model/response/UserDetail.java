package com.dataignyte.popolr.retrofit.model.response;

import com.dataignyte.popolr.helper.Utility;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UserDetail {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("email")
    @Expose
    private String email;

    public String getId() {
        return Utility.geNonNullString(id);
    }

    public String getFullName() {
        return Utility.geNonNullString(fullName);
    }

    public String getFirstName() {
        return Utility.geNonNullString(firstName);
    }

    public String getLastName() {
        return Utility.geNonNullString(lastName);
    }

    public String getGender() {
        return Utility.geNonNullString(gender);
    }

    public String getPhoneNumber() {
        return Utility.geNonNullString(phoneNumber);
    }

    public String getEmail() {
        return Utility.geNonNullString(email);
    }
}
