package com.dataignyte.popolr.retrofit.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DeliverySlotsResponse {

    @SerializedName("deliverySlots")
    @Expose
    ArrayList<DeliverySlotModal> deliverySlots;

    public ArrayList<DeliverySlotModal> getDeliverySlots() {
        return deliverySlots;
    }
}
