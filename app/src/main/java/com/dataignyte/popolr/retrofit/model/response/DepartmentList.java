package com.dataignyte.popolr.retrofit.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DepartmentList {
    @SerializedName("departments")
    @Expose
    private ArrayList<Department> departments = null;

    public ArrayList<Department> getDepartments() {
        return departments;
    }
}
