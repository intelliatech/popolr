package com.dataignyte.popolr.retrofit.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CategoryList {
    @SerializedName("categories")
    @Expose
    private ArrayList<Category> categories = null;

    public ArrayList<Category> getCategories() {
        return categories;
    }
}
