package com.dataignyte.popolr.retrofit.model.response;

import com.dataignyte.popolr.helper.Utility;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UserDetailsResponse {
    @SerializedName("_id")
    String _id;
    @SerializedName("fullName")
    String fullName;
    @SerializedName("firstName")
    String firstName;
    @SerializedName("lastName")
    String lastName;
    @SerializedName("gender")
    String gender;
    @SerializedName("phoneNumber")
    long phoneNumber;
    @SerializedName("email")
    String email;
    @SerializedName("orderTotal")
    double orderTotal;
    @SerializedName("rewardPoints")
    int rewardPoints;
    @SerializedName("active")
    boolean active;

    @SerializedName("childs")
    ArrayList<UserDetailsResponse> child;

    @SerializedName("direct")
    int direct;

    @SerializedName("subCordinates")
    int subCordinates;

    public String get_id() {
        return _id;
    }

    public String getFullName() {
        return Utility.geNonNullString(fullName);
    }

    public String getFirstName() {
        return Utility.geNonNullString(firstName);
    }

    public String getLastName() {
        return Utility.geNonNullString(lastName);
    }

    public String getGender() {
        return Utility.geNonNullString(gender);
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return Utility.geNonNullString(email);
    }

    public double getOrderTotal() {
        return orderTotal;
    }

    public int getRewardPoints() {
        return rewardPoints;
    }

    public boolean isActive() {
        return active;
    }

    public ArrayList<UserDetailsResponse> getChild() {
        return child;
    }

    public int getDirect() {
        return child.size();
    }

    public int getSubCordinates() {
        return 0;
    }

}
