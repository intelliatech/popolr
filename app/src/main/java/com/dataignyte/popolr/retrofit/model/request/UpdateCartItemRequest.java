package com.dataignyte.popolr.retrofit.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UpdateCartItemRequest {
    @SerializedName("items")
    @Expose
    private ArrayList<Item> items = null;

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public static class Item {
        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("quantity")
        @Expose
        private int quantity;

        public void setId(String id) {
            this.id = id;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

    }
}
