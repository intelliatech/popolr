package com.dataignyte.popolr.retrofit.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OrderListResponse {
    @SerializedName("orders")
    @Expose
    ArrayList<OrderListModel> orders;

    public ArrayList<OrderListModel> getOrders() {
        return orders;
    }
}
