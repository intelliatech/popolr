package com.dataignyte.popolr.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PaymentModesResponse {
    @SerializedName("paymentModes")
    @Expose
    ArrayList<PaymentModeModel> paymentModes;

    public ArrayList<PaymentModeModel> getPaymentModes() {
        return paymentModes;
    }
}
