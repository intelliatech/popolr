package com.dataignyte.popolr.retrofit.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderItem {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("item")
    @Expose
    private ItemDetails item;
    @SerializedName("quantity")
    @Expose
    private int quantity;
    @SerializedName("total")
    @Expose
    private float total;
    @SerializedName("discountTotal")
    @Expose
    private float discountTotal;

    public String getId() {
        return id;
    }

    public ItemDetails getItem() {
        return item;
    }

    public int getQuantity() {
        return quantity;
    }

    public float getTotal() {
        return total;
    }

    public float getDiscountTotal() {
        return discountTotal;
    }
}
