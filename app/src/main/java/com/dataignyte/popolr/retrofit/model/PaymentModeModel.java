package com.dataignyte.popolr.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentModeModel {

    @SerializedName("_id")
    @Expose
    String _id;

    @SerializedName("name")
    @Expose
    String name;

    public String get_id() {
        return _id;
    }

    public String getName() {
        return name;
    }
}
