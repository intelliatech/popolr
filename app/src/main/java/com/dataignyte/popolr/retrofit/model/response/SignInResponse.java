package com.dataignyte.popolr.retrofit.model.response;

import com.dataignyte.popolr.helper.Utility;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SignInResponse {
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("token")
    @Expose
    private String token;

    public String getMessage() {
        return Utility.geNonNullString(message);
    }

    public String getToken() {
        return Utility.geNonNullString(token);
    }
}
