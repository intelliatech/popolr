package com.dataignyte.popolr.retrofit.model.response;

import com.dataignyte.popolr.helper.Utility;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddressModal {

    @SerializedName("_id")
    @Expose
    String _id;

    @SerializedName("user")
    @Expose
    String user;

    @SerializedName("line1")
    @Expose
    String line1;

    @SerializedName("line2")
    @Expose
    String line2;

    @SerializedName("city")
    @Expose
    String city;

    @SerializedName("state")
    @Expose
    String state;

    @SerializedName("pincode")
    @Expose
    String pincode;

    @SerializedName("type")
    @Expose
    String type = "Home";

    public String getLine1() {
        return Utility.geNonNullString(line1);
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return Utility.geNonNullString(line2);
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getCity() {
        return Utility.geNonNullString(city);
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return Utility.geNonNullString(state);
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return Utility.geNonNullString(pincode);
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getType() {
        return Utility.geNonNullString(type);
    }

    public void setType(String type) {
        this.type = type;
    }

    public String get_id() {
        return Utility.geNonNullString(_id);
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUser() {
        return Utility.geNonNullString(user);
    }

    public void setUser(String user) {
        this.user = user;
    }
}
