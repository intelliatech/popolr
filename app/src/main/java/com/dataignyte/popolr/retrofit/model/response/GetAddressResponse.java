package com.dataignyte.popolr.retrofit.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetAddressResponse {

    @SerializedName("addresses")
    @Expose
    ArrayList<AddressModal> addresses;

    public ArrayList<AddressModal> getAddresses() {
        return addresses;
    }
}
