package com.dataignyte.popolr.retrofit.APICaller;

import com.dataignyte.popolr.Constants;
import com.dataignyte.popolr.retrofit.model.CreateOrderRequest;
import com.dataignyte.popolr.retrofit.model.PaymentModesResponse;
import com.dataignyte.popolr.retrofit.model.request.ChangePasswordRequest;
import com.dataignyte.popolr.retrofit.model.request.ReferralUserRequest;
import com.dataignyte.popolr.retrofit.model.request.SignInRequest;
import com.dataignyte.popolr.retrofit.model.request.SignUpRequest;
import com.dataignyte.popolr.retrofit.model.request.UpdateCartItemRequest;
import com.dataignyte.popolr.retrofit.model.response.AddressModal;
import com.dataignyte.popolr.retrofit.model.response.CartResponse;
import com.dataignyte.popolr.retrofit.model.response.CategoryList;
import com.dataignyte.popolr.retrofit.model.response.DeliverySlotsResponse;
import com.dataignyte.popolr.retrofit.model.response.DepartmentList;
import com.dataignyte.popolr.retrofit.model.response.GetAddressResponse;
import com.dataignyte.popolr.retrofit.model.response.ItemDetails;
import com.dataignyte.popolr.retrofit.model.response.ItemsList;
import com.dataignyte.popolr.retrofit.model.response.OrderDetailsModel;
import com.dataignyte.popolr.retrofit.model.response.OrderListResponse;
import com.dataignyte.popolr.retrofit.model.response.SignInResponse;
import com.dataignyte.popolr.retrofit.model.response.SubCategoryList;
import com.dataignyte.popolr.retrofit.model.response.SucessResult;
import com.dataignyte.popolr.retrofit.model.response.UserDetail;
import com.dataignyte.popolr.retrofit.model.response.UserDetailsResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET(Constants.URL.GET_DEPARTMENTS)
    Call<DepartmentList> callGetAllDepartmentAPI();

    @GET(Constants.URL.GET_SPECIAL_ITEMS)
    Call<ItemsList> getSpecialItemsAPI();

    @GET(Constants.URL.GET_CATEGORY_LIST_BY_DEPARTMENT + "/{id}")
    Call<CategoryList> callCategoryListByDepartment(@Path("id") String id);

    @GET(Constants.URL.GET_SUB_CATEGORY_LIST_BY_CATEGORY + "/{id}")
    Call<SubCategoryList> getSubCategoryListByCategoryAPI(@Path("id") String id);

    @GET(Constants.URL.REGISTER_USER + "/{mobileNo}/otp/password")
    Call<SucessResult> sendOTPtoChangePasswordAPI(@Path("mobileNo") String mobileNo);

    @GET(Constants.URL.GET_ITEM_LIST_BY_SUB_CATEGORY + "/{id}")
    Call<ItemsList> getItemListBySubCategoryAPI(@Path("id") String id);

    @GET(Constants.URL.GET_SEARCH_ITEM_BY_NAME)
    Call<ItemsList> searchItemByNameAPI(@Query("search") String search);

    @GET(Constants.URL.GET_ITEM_DETAILS + "/{id}")
    Call<ItemDetails> getItemDetailsAPI(@Path("id") String id);

    @POST(Constants.URL.REGISTER_USER)
    Call<UserDetail> callRegisterUserAPI(@Body SignUpRequest requestObject);

    @POST(Constants.URL.CHANGE_PASSWORD)
    Call<SucessResult> callChangePasswordAPI(@Body ChangePasswordRequest requestObject);

    @POST(Constants.URL.USER_lOGIN)
    Call<SignInResponse> callSignInAPI(@Body SignInRequest signInRequest);

    @POST(Constants.URL.ADD_ADDRESS)
    Call<AddressModal> callAddAddressAPI(@Header(Constants.AUTHORIZATION) String token, @Body AddressModal addressRequest);

    @GET(Constants.URL.GET_ADDRESSES)
    Call<GetAddressResponse> getMyAddressess(@Header(Constants.AUTHORIZATION) String token);

    @DELETE(Constants.URL.DELETE_ADDRESS + "/{id}")
    Call<SucessResult> callDeleteAddressAPI(@Header(Constants.AUTHORIZATION) String token, @Path("id") String id);

    @GET(Constants.URL.GET_CART)
    Call<CartResponse> getMyCart(@Header(Constants.AUTHORIZATION) String token);

    @GET(Constants.URL.GET_USER_DETAILS)
    Call<UserDetailsResponse> getUserDetails(@Header(Constants.AUTHORIZATION) String token);

    @GET(Constants.URL.GET_DELIVERY_SLOTS)
    Call<DeliverySlotsResponse> getDeliverySlots();

    @GET(Constants.URL.GET_PAYMENT_MODES)
    Call<PaymentModesResponse> getPaymentModes();

    @POST(Constants.URL.GET_CART)
    Call<SucessResult> addUpdateCart(@Header(Constants.AUTHORIZATION) String token, @Body UpdateCartItemRequest updateCartItemRequest);

    @POST(Constants.URL.ORDER_URL)
    Call<SucessResult> callCreateOrderAPI(@Header(Constants.AUTHORIZATION) String token, @Body CreateOrderRequest updateCartItemRequest);

    @GET(Constants.URL.ORDER_URL)
    Call<OrderListResponse> getMyOrdersAPI(@Header(Constants.AUTHORIZATION) String token);

    @GET(Constants.URL.ORDER_URL + "/{id}")
    Call<OrderDetailsModel> getOrderDetailsAPI(@Header(Constants.AUTHORIZATION) String token, @Path("id") String id);


    @POST (Constants.URL.REGISTER_USER)
    Call<SucessResult> createReferralUser(@Body ReferralUserRequest referralUserRequest);


}