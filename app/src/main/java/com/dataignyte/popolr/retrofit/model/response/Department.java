package com.dataignyte.popolr.retrofit.model.response;

import com.dataignyte.popolr.helper.Utility;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Department {

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("images")
    @Expose
    private List<String> images = null;

    @SerializedName("categoryCount")
    @Expose
    private int categoryCount;

    public String getId() {
        return Utility.geNonNullString(id);
    }

    public String getName() {
        return Utility.geNonNullString(name);
    }

    public List<String> getImages() {
        return images;
    }

    public int getCategoryCount() {
        return categoryCount;
    }
}
