package com.dataignyte.popolr.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateOrderRequest {
    @SerializedName("deliverySlot")
    @Expose
    String deliverySlot;

    @SerializedName("paymentMode")
    @Expose
    String paymentMode;

    @SerializedName("address")
    @Expose
    String address;

    public void setDeliverySlot(String deliverySlot) {
        this.deliverySlot = deliverySlot;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
