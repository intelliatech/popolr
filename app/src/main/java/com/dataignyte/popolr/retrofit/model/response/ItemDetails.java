package com.dataignyte.popolr.retrofit.model.response;

import com.dataignyte.popolr.helper.Utility;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ItemDetails {
    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("sku")
    @Expose
    private String sku;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("unit")
    @Expose
    private String unit;

    @SerializedName("mrp")
    @Expose
    private float mrp;

    @SerializedName("sellPrice")
    @Expose
    private float sellPrice;

    @SerializedName("discountPercent")
    @Expose
    private double discountPercent;

    @SerializedName("discountTotal")
    @Expose
    private double discountTotal;

    @SerializedName("images")
    @Expose
    private ArrayList<String> images = null;

//    @SerializedName("subCategory")
//    @Expose
//    private SubCategory subCategory;

    @SerializedName("category")
    @Expose
    private Category category;

//    @SerializedName("brand")
//    @Expose
//    private Category brand;

    @SerializedName("department")
    @Expose
    private Department department;

    @SerializedName("comparable")
    @Expose
    private boolean comparable;

    @SerializedName("quantity")
    @Expose
    private int quantity = 0;

    @SerializedName("isAddVisible")
    @Expose
    private boolean isAddButtonVisible=true;

    public boolean isAddButtonVisible() {
        return isAddButtonVisible;
    }

    public void setAddButtonVisible(boolean addButtonVisible) {
        isAddButtonVisible = addButtonVisible;
    }

    public ItemDetails() {
    }

    public String getId() {
        return Utility.geNonNullString(id);
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return Utility.geNonNullString(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSku() {
        return Utility.geNonNullString(sku);
    }

    public String getDescription() {
        return Utility.geNonNullString(description);
    }

    public String getUnit() {
        return Utility.geNonNullString(unit);
    }

    public float getMrp() {
        return mrp;
    }

    public float getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(float sellPrice) {
        this.sellPrice = sellPrice;
    }

    public double getDiscountPercent() {
        return discountPercent;
    }

    public double getDiscountTotal() {
        return discountTotal;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public Category getCategory() {
        return category;
    }

//    public Category getBrand() {
//        return brand;
//    }

    public Department getDepartment() {
        return department;
    }

    public boolean isComparable() {
        return comparable;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

//    public SubCategory getSubCategory() {
//        return subCategory;
//    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return "ItemDetails{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
