package com.dataignyte.popolr.retrofit.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangePasswordRequest {
    @SerializedName("passowrd")
    @Expose
    private String passowrd;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("otp")
    @Expose
    private String otp;

    public void setPassowrd(String passowrd) {
        this.passowrd = passowrd;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
