package com.dataignyte.popolr.retrofit.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrdersHeader {
    @SerializedName("ordersCount")
    @Expose
    String ordersCount;
    @SerializedName("ordersDate")
    @Expose
    String ordersDate;

    public OrdersHeader(String ordersCount, String ordersDate) {
        this.ordersCount = ordersCount;
        this.ordersDate = ordersDate;
    }

    public String getOrdersCount() {
        return ordersCount;
    }

    public void setOrdersCount(String ordersCount) {
        this.ordersCount = ordersCount;
    }

    public String getOrdersDate() {
        return ordersDate;
    }

    public void setOrdersDate(String ordersDate) {
        this.ordersDate = ordersDate;
    }
}
